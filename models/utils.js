
const neo4j = require('neo4j-driver')
const con = require('./const')
const utils = require('./utils')
const driver = neo4j.driver(con.DB_ADDRESS, neo4j.auth.basic('neo4j', con.DB_PASSWD),
    { disableLosslessIntegers: true })

module.exports = {
    extractBasic: function(msg){
        return {
            msg_id: msg.msg_id,
            operation: msg.operation,
            user: msg.user,
            user_id: msg.user_id,
            project: msg.project,
            error: false
        }
    },
    generateError: function (resp,err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.error_str = JSON.stringify(err);
        resp.error_detail = err;
        return resp;
    },
    readCypher: async function(cypher){
        let session = driver.session();
        try{
            let res =  await session.readTransaction(async txc =>{
                let result = txc.run(cypher);
                return result;
            });
            return res;
        }finally {
            session.close();
        }
    },
    writeCypher: async function(cypher){
        let session = driver.session();
        try{
            let res =  await session.writeTransaction(async txc =>{
                let result = txc.run(cypher);
                return result;
            })
            return res;
        }finally {
            session.close();
        }
    },
    driver: driver
}
