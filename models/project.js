"use strict"
const {extractBasic,readCypher,writeCypher,driver,generateError} = require("./utils");

function DataManager(){}

DataManager.prototype.initDatabase = async function (msg) {
    var session = driver.session()
    var resp = extractBasic(msg)
    try{
        await session.writeTransaction(async txc=>{
            await txc.run('MATCH (n) DETACH DELETE n')
            //内置属性类型:int/float/string
            let res = await txc.run('Create (s:SymbolType {name:"Int",type:"Basic"}) Return id(s) as symbolId');
            resp["int_id"] = res.records[0].get("symbolId");
            res = await txc.run('Create (s:SymbolType {name:"String",type:"Basic"}) Return id(s) as symbolId');
            resp["string_id"] = res.records[0].get("symbolId");
            res = await txc.run('Create (s:SymbolType {name:"Float",type:"Basic"}) Return id(s) as symbolId');
            resp["float_id"] = res.records[0].get("symbolId");
        })
        await session.writeTransaction(async txc=>{
            await txc.run('CREATE CONSTRAINT ON (u:User) ASSERT u.name IS UNIQUE');
            await txc.run('CREATE CONSTRAINT ON (p:Project) ASSERT p.name IS UNIQUE');
            await txc.run('CREATE CONSTRAINT ON (s:SymbolType) ASSERT s.name IS UNIQUE')
        })

    }catch(err){
        generateError(resp,err);
    }finally {
        await session.close()
    }
    return resp
}

DataManager.prototype.createUser = async function (msg) {
    var session = driver.session();
    var resp = extractBasic(msg);
    try{
        await session.writeTransaction(async txc=>{
            let cypher = `Match (u:User {name: '${msg.user}'}) Return u`;
            let res=await txc.run(cypher);
            if(res.records!=0) throw Error("该用户名已存在");
            cypher = `Create (u:User {name: '${msg.user}', passwd:'${msg.passwd}'})`;
            txc.run(cypher)
        })
    }catch(err){
        generateError(resp,err);
    }finally {
        await session.close()
    }
    return resp
}

DataManager.prototype.checkUser = async function (msg) {
    var session = driver.session();
    var resp = extractBasic(msg);
    try{
        let res = await session.run('MATCH (u:User {name : {name},\
                            passwd: {passwd}}) \
                            RETURN id(u) AS uid', {
            name: msg.user,
            passwd: msg.passwd,
        })
        let records = res.records;
        if (records.length != 0)
        {
            resp.msg = 'Success'
            resp.user_id = records[0].get('uid')
        }
        else
        {
            resp.error = true;
            resp.msg = "User not exists";
        }
    }catch(err){
        generateError(resp,err);
    }finally {
        await session.close()
    }
    return resp
}

DataManager.prototype.createProject = async function (msg) {
    var resp = extractBasic(msg);
    let cypher = `CREATE (p:Project {name : "${msg.project}"})`
    try{
        await writeCypher(cypher)
    }catch(err){
        generateError(resp,err);
    }
    return resp
}

DataManager.prototype.getProjects = async function(msg){
    var resp = extractBasic(msg);
    try{
        let cypher = `Match (p:Project)
        Match (u:User) Where id(u)=${msg.user_id}
        Return p.name as pname,u.name as uname`;
        let res = await readCypher(cypher);
        let plist = [];
        for(let p of res.records){
            plist.push(p.get("pname"));
        }
        resp["projects"] = plist;
        resp["user_name"] = res.records[0].get("uname")
    }catch(err){
        generateError(resp,err);
    }
    return resp;
};





module.exports = DataManager;