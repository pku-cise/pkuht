"use strict"
const {extractBasic,readCypher,writeCypher,driver,generateError} = require("./utils");

function DataManager(){}


let extractInst = function(inst)
{
    let result = {
        "id": inst.identity,
        "properties":{}
    };
    for(let [key,value] of Object.entries(inst.properties)){
        if(key==="name") result["name"] = value;
        else{
            //特殊符号处理
            value = value.replace("\n", "\\n");
            result["properties"][key] = JSON.parse(value)
        }    
    
    }
    return result;
};

let refer = async function (userid, nodeid, txc, refer_name=null) {
    if(refer_name!=null){
        refer_name = `"${refer_name}"`
    }else{
        refer_name = `n.name`
    }
    let cypher = `MATCH (u) WHERE id(u) = ${userid}
    MATCH (n) WHERE id(n) = ${nodeid} and not (u) - [:refer] - (n) 
    Optional Match (u) - [rej:reject] - (n) 
    Delete rej
    Create (u)-[:refer {name:${refer_name}}]->(n)
    Return n.name`;
    let res = await txc.run(cypher)
    if(res.records.length===0){
        throw Error(`添加的实例已经在当前的知识图谱中,id为${nodeid}`);
    }
}

DataManager.prototype.createEntity = async function (msg) {
    var session = driver.session();
    var resp = extractBasic(msg);
    try{
        await session.writeTransaction(async txc => {
            let cypher = `MATCH (p:Project {name: "${msg.project}"})
            MATCH (p)-[:has]->(et:EType) Where id(et)=${msg.entity_type_id}
            Match (:User) - [:refer {name: "${msg.entity_name}"}] -> (e:Entity) - [:inst_of]->(et) 
            RETURN DISTINCT e AS entity`;
            let res=await txc.run(cypher);
            let entity,result;
            if(res.records.length>1){
                throw Error(`存在重复的实例，请联系管理员(助教)`);
            }
            else if(res.records.length>=1){
                entity = res.records[0].get('entity');
                result = extractInst(entity);
                resp = Object.assign(resp,result);
            }else{
                cypher = `MATCH (p:Project {name: "${msg.project}"})
                MATCH (p)-[:has]->(et:EType) Where id(et)=${msg.entity_type_id}
                Create (e:Inst:Entity {name:"${msg.entity_name}"}) - [:inst_of] -> (et)
                Create (p) - [:has] -> (e)
                Return e AS entity`;
                res = await txc.run(cypher);
                if(res.records.length === 0) throw Error("创建错误，请检查该实体类型是否存在");
                entity = res.records[0].get('entity');
                result = extractInst(entity);
                result["name"] = msg.entity_name;
                resp = Object.assign(resp,result);
            }
            await refer(msg.user_id, result["id"], txc)
        })

    }catch(err){
        generateError(resp,err);
    }finally {
        await session.close()
    }
    return resp
}


DataManager.prototype.createRelation = async function (msg) {
    var session = driver.session();
    var resp = extractBasic(msg);
    try{
        let cypher = `MATCH (p:Project {name: "${msg.project}"})\
        MATCH (p)-[:has]->(r:Relation)\
        MATCH  (r)-[:inst_of] -> (re:RType) Where id(re)=${msg.relation_type_id} `;
        let i = 0;
        for(let role of msg.roles){
            cypher+=`MATCH (r) - [:${role.role}] -> (inst${i}:Inst) WHERE id(inst${i++})=${role.inst_id} `
        }
        //确保匹配到的是该关系的所有角色
        cypher+=`Match (r) - [] -> (inst:Inst)
        With r,count(inst) as cnt
        Where cnt = ${i}
        Return r as relation`;
        await session.writeTransaction(async txc => {
            let res=await txc.run(cypher);
            let relation;
            if(res.records.length==0) {
                let cypher = `MATCH (p:Project {name: "${msg.project}"})\
                MATCH (p)-[:has] -> (re:RType) Where id(re)=${msg.relation_type_id}\
                CREATE (p)-[:has]->(r:Inst:Relation)\
                CREATE (r)-[:inst_of] -> (re) `
                let i = 0;
                for(let role of msg.roles){
                    //Todo 增加类型校验
                    cypher+=`WITH r\
                            MATCH (e${i}:Inst) WHERE id(e${i})=${role.inst_id}\
                            CREATE (r) - [:${role.role}] -> (e${i++}) `
                }
                cypher+='RETURN r AS relation';
                res = await txc.run(cypher);
                relation = res.records[0].get('relation')
            }else {
                relation = res.records[0].get('relation')
            }
            let result = extractInst(relation)
            resp = Object.assign(resp,result)
            await refer(msg.user_id, result['id'], txc)
        })

    }catch(err){
        generateError(resp,err)
    }finally {
        await session.close()
    }
    return resp
}

DataManager.prototype.createConstraint = async function (msg) {
    var session = driver.session();
    var resp = extractBasic(msg);
    try{
        let cypher = ``;

        if(("precond_item_list" in msg) && (msg.precond_item_list.length > 0)){
            for(let precond_item of msg.precond_item_list){
                if(("precond_list" in precond_item) && (precond_item.precond_list.length > 0)){
                    for(let precond of precond_item.precond_list){
                        if(("args" in precond) && (precond.args.length > 1)){}
                        else {throw Error("createConstraint: precond args length error.");}

                        if("opt" in precond) {}
                        else {throw Error("createConstraint: precond NO opt.");}
                    }
                }else{
                    throw Error("createConstraint: precond_item NO precond_list.");
                }
            }
        }else{
            throw Error("createConstraint: NO precond_item_list.");
        }

        if(("postcond_item_list" in msg) && (msg.postcond_item_list.length > 0)){
            for(let postcond_item of msg.postcond_item_list){
                if(("postcond_list" in postcond_item) && (postcond_item.postcond_list.length > 0)){
                    for(let postcond of postcond_item.postcond_list){
                        if(("args" in postcond) && (postcond.args.length > 1)){}
                        else {throw Error("createConstraint: postcond args length error.");}

                        if("opt" in postcond) {}
                        else {throw Error("createConstraint: postcond NO opt.");}
                    }
                }else{
                    throw Error("createConstraint: postcond_item NO postcond_list.");
                }
            }
        }else{
            throw Error("createConstraint: NO postcond_item_list.");
        }

        cypher = `MATCH (p:Project {name: "${msg.project}"}) \
            CREATE (p)-[:has]->(c:Constraint:Inst) `

        cypher += `Set c.precond_item_list = '${JSON.stringify(msg.precond_item_list)}' `
        cypher += `Set c.postcond_item_list = '${JSON.stringify(msg.postcond_item_list)}' `

        for(let precond_item of msg.precond_item_list){
            for(let precond of precond_item.precond_list){
                for(let arg of precond.args){
                    if(arg.type=="node"){
                        cypher += `with p,c \
                            MATCH (i:Inst) where id(i)=${arg.inst_id} \ 
                            Create (c)-[:precond]->(i) `
                    }
                    else if(arg.type=="number"){
                    }
                    //else if(arg.type=="int"){}
                    //else if(arg.type=="float"){}
                    else if(arg.type=="bool"){
                    }
                    else if(arg.type=="patient"){
                    }
                    else if(arg.type=="string"){
                    }
                }
            }
        }

        for(let postcond_item of msg.postcond_item_list){
            for(let postcond of postcond_item.postcond_list){
                for(let arg of postcond.args){
                    if(arg.type=="node"){
                        cypher += `with p,c \
                            MATCH (i:Inst) where id(i)=${arg.inst_id} \ 
                            Create (c)-[:postcond]->(i) `
                    }
                    else if(arg.type=="number"){
                    }
                    //else if(arg.type=="int"){}
                    //else if(arg.type=="float"){}
                    else if(arg.type=="bool"){
                    }
                    else if(arg.type=="patient"){
                    }
                    else if(arg.type=="string"){
                    }
                }
            }
        }
        cypher += `RETURN c as constraint `;
        await session.writeTransaction(async txc => {
            let res = await txc.run(cypher);

            let record = res.records[0];
            let constraint = extractInst(record.get("constraint"))

            await refer(msg.user_id, constraint['id'], txc)

            resp["id"] = constraint["id"];
            resp["precond_item_list"] = constraint["properties"]["precond_item_list"]
            resp["postcond_item_list"] = constraint["properties"]["postcond_item_list"]
        })

    }catch(err){
        generateError(resp,err);
    }finally {
        await session.close()
    }
    return resp
};

let getEntityTypes = async function(project,user_id,txc){
    let cypher = `Match (p:Project {name: "${project}"})
    Match (u:User) where id(u) = ${user_id}
    Match (p) - [:has] -> (et:EType) Where not ( (u) - [:reject] -> (et) )
    Optional match (et) - [hp:has_property] - (st:SymbolType) 
    Return et,collect (distinct[hp,st]) as properties`;
    let res = await txc.run(cypher)
    let ets = [];
    for (let et of res.records){
        let etype={
            "id": et._fields[0].identity,
            "name":et._fields[0].properties.name,
            "properties":{}
        };
        for(let property of et.get("properties")){
            if(property[0] === null) continue;
            etype["properties"][property[0].properties.name]={
                "type_id":property[1].identity,
                "type_name":property[1].properties.name
            }
        }
        ets.push(etype);
    }
    return ets;
}

let getRoleTypes = async function(project,user_id,txc){
    let cypher = `Match (p:Project {name: "${project}"})
    Match (u:User) where id(u) = ${user_id}
    Match (p) - [:has] -> (rt:RoleType) Where not ( (u) - [:reject] -> (rt) )
    Optional match (rt) - [hp:has_property] - (st:SymbolType) 
    Return rt,collect (distinct[hp,st]) as properties`;
    let res = await txc.run(cypher)
    let rts = [];
    for (let rt of res.records){
        let type_ids = JSON.parse(rt._fields[0].properties.name)["ids"];
        let roletype={
            "id": rt._fields[0].identity,
            //"name":rt._fields[0].properties.name,
            //"properties":{}
            "type_ids": type_ids
        };
        /*
        for(let property of rt.get("properties")){
            if(property[0] === null) continue;
            roletype["properties"][property[0].properties.name]={
                "type_id":property[1].identity,
                "type_name":property[1].properties.name
            }
        }
        */
        rts.push(roletype);
    }
    return rts;
}

let getRelationTypes = async function(project,user_id,txc){
    let cypher = `Match (p:Project {name: "${project}"})
    Match (u:User) where id(u) = ${user_id}
    Match (p) - [:has] -> (rt:RType) Where not ( (u) - [:reject] -> (rt) ) 
    Match (rt) - [hr:has_role] -> (et:Type)
    Optional match (rt) - [hp:has_property] - (st:SymbolType)
    Return rt,collect(distinct [hr, et]) AS roles,collect (distinct[hp,st]) as properties`
    let res = await txc.run(cypher)
    let rts = [];
    for (let rt of res.records){
        let rt_obj  = {
            "id":rt._fields[0].identity,
            "name":rt._fields[0].properties.name,
            "roles":{},
            "properties":{},
        }
        for(let role of rt._fields[1]){
            rt_obj["roles"][role[0].properties.name] = {
                "type_id":role[1].identity,
                "type_name":role[1].properties.name,
                "min": role[0].properties.min,
                "max": role[0].properties.max,
            }
        }
        for(let property of rt.get("properties")){
            if(property[0] === null) continue;
            rt_obj["properties"][property[0].properties.name]={
                "type_id":property[1].identity,
                "type_name":property[1].properties.name
            }
        }
        rts.push(rt_obj);
    }
    return rts;
}

//这个函数被用于抽取对一组entity的查询信息，其cypher返回语句应类似：
//Return e,id(et) as typeId, ref.name as refName（这一项为可选项，被用于用户给实体的别名)`
let dealEntityQueryGroup= function(res)
{
    let ens = [];
    for (let en of res.records){
        let result = extractInst(en._fields[0]);
        ens.push({
            "id":result["id"],
            "type_id":en._fields[1],
            //当抽取用户个人知识图谱时，这一项为用户认为这个实体的名字
            "name":en.keys.length>2&&en.keys[2]==='refName'? en._fields[2]:result["name"],
            "properties":result["properties"]
        });
    }
    return ens;
};

//这个函数被用于抽取对一组relation的查询信息，其cypher返回语句应类似：
//Return r,id(rt) as typeId, collect(distinct[role,i]) as roles`
let dealRelationQueryGroup = function(res){
    let relas = [];
    for (let rela of res.records){
        let result  = extractInst(rela._fields[0])
        let rela_inst = {
            "id": result["id"],
            "type_id":rela.get("typeId"),
            "properties":result["properties"],
            "roles":[]
        };
        for(let role of rela._fields[2]){
            rela_inst["roles"].push({"role":role[0].type,"inst_id":role[1].identity})
        }
        //Todo 妥协方案，用来获取推荐优先级，之后会用列表中的顺序替代
        if(rela.has("priority")) rela_inst["priority"] = rela.get("priority");
        relas.push(rela_inst);
    }
    return relas;
};

//Return c as constraint;
let dealConstraintQueryGroup = function(res){
    let constraints = []
    for (let record of res.records)
    {
        let constraint = extractInst(record.get("constraint"))
        let res = {}
        res["id"] = constraint["id"]
        res["precond_item_list"] = constraint["properties"]["precond_item_list"]
        res["postcond_item_list"] = constraint["properties"]["postcond_item_list"]
        constraints.push(res)
    }
    return constraints
};


let getEntities = async function(project,userid,txc){
    let cypher = `Match (p:Project {name: "${project}"})
    Match (u:User) Where id(u)=${userid}
    Match (p) - [:has] -> (e:Entity) <- [ref:refer] - (u)
    Match (e) - [:inst_of] -> (et:EType)
    Return e,id(et) as typeId, ref.name as refName`
    let res = await txc.run(cypher)
    return dealEntityQueryGroup(res);
}

let getRelations = async function(project,userid,txc){
    let cypher = `Match (p:Project {name: "${project}"})
    Match (u:User) Where id(u)=${userid}
    Match (p) - [:has] -> (r:Relation) <- [:refer] - (u)
    Match (r) - [:inst_of] -> (rt:RType)
    Match (r) - [role] -> (e:Inst)
    Return r,id(rt) as typeId, collect(distinct[role,e]) as roles`;
    let res = await txc.run(cypher)
    return dealRelationQueryGroup(res);
}

let getConstraints = async function(project,userid,txc){
    let cypher = `Match (p:Project {name: "${project}"})
    Match (u:User) Where id(u)=${userid}
    Match (p) - [:has] -> (c:Constraint) <- [:refer] - (u)
    Return c as constraint `;
    let res = await txc.run(cypher);
    return dealConstraintQueryGroup(res);
};

let getSymbolTypes = async function(txc){
    let cypher = `Match (s:SymbolType)
    Optional match (s) - [hc:has_child_symbol] -> (cs:SymbolType)
    Return s,collect(distinct[hc.name,id(cs)]) as childs`;
    let symbol_types = [];
    let res = await txc.run(cypher);
    for(let st of res.records){
        let id = st.get("s").identity;
        let symbol_type = st.get("s").properties;
        symbol_type["id"] = id;
        for(let child of st.get("childs")){
            if(child[0]===null) break;
            if(symbol_type.child_symbols===undefined) symbol_type.child_symbols={};
            symbol_type.child_symbols[child[0]] = child[1];
        }
        symbol_types.push(symbol_type);
    }
    return symbol_types;
}

DataManager.prototype.getGraph = async function(msg){
    var session = driver.session();
    var resp = extractBasic(msg);
    try{
        await session.readTransaction(async (txc)=>{
            resp["entity_types"] = await getEntityTypes(msg.project,msg.user_id,txc);
            resp["relation_types"] = await getRelationTypes(msg.project,msg.user_id,txc);
            resp["role_types"] = await getRoleTypes(msg.project,msg.user_id,txc);
            resp["entities"] = await getEntities(msg.project, msg.user_id, txc);
            resp["relations"] = await getRelations(msg.project, msg.user_id, txc);
            resp["constraints"] = await getConstraints(msg.project, msg.user_id, txc)
            resp["symbol_types"] = await getSymbolTypes(txc)
        })
    }catch(err){
        generateError(resp,err)
    }finally {
        await session.close()
    }
    return resp;
};


let getSubgraphEntities = async function(project,userid,entities,txc){
    let cypher = ``;
    for(let i=0; i<entities.length; i++)
        for(let j=i+1; j<entities.length; j++)
        {
            cypher = `Match (p:Project {name: "${project}"})
            Match (u:User) Where id(u)=${userid}
            Match (p) - [:has] -> (e:Entity) <- [ref:refer] - (u)
            Match (e) - [:inst_of] -> (et:EType)
            Return e,id(et) as typeId, ref.name as refName`
        }
}

//msg["entities"] = [{"id": 112}, ..]
DataManager.prototype.getSubgraph = async function(msg){
    var session = driver.session();
    var resp = extractBasic(msg);
    try{
        await session.readTransaction(async (txc)=>{
            let relationTypes = getRelationTypes(msg.project,msg.user_id,txc);

            resp["entities"] = await getSubgraphEntities(msg.project, msg.user_id, msg.entities, txc);
            //resp["relations"] = await getSubgraphRelations(msg.project, msg.user_id, txc);
            //resp["constraints"] = await getSubgraphConstraints(msg.project, msg.user_id, txc);
        })
    }catch(err){
        generateError(resp,err);
    }finally {
        await session.close()
    }
    return resp;
}




DataManager.prototype.setInstanceProperty = async function(msg){
    var session = driver.session();
    var resp = extractBasic(msg);
    try{
        let cypher = `Match (n:Inst) where id(n)=${msg["inst_id"]} `
        for(let key in msg["properties"]){
            let value = msg["properties"][key];
            if(value===null){
                cypher += `Set n.${key} = null `;
                continue;
            }
            cypher += `Set n.${key} = '${JSON.stringify(value)}'`
        }
        await session.run(cypher)
    }catch(err){
        generateError(resp,err);
    }finally {
        await session.close()
    }
    return resp;
};

let deleteInstance = async function(user_id,inst_id){
    //有向图，边的方向
    let cypher = `Match (u:User) - [ref:refer] - (i:Inst)
                      Where id(u)=${user_id} and id(i)=${inst_id}
                      Optional Match (u) - [ref_r:refer] -> (r:Relation) - [*1..] -> (i) 
                      Optional Match (u) - [ref_c:refer] -> (c:Constraint) - [*1..] -> (i)
                      Delete ref,ref_r,ref_c
                      Return ref,ref_r,ref_c`
    let res =await writeCypher(cypher)
    if(res.records.length==0) {
        throw new Error(`refer not exists`);
    }
};

DataManager.prototype.deleteEntity = async function(msg){
    var resp = extractBasic(msg);
    try{
        await deleteInstance(msg.user_id,msg.entity_id)
    }catch(err){
        generateError(resp,err)
    }
    return resp
}

DataManager.prototype.deleteRelation = async function(msg){
    var resp = extractBasic(msg);
    try{
        await deleteInstance(msg.user_id,msg.relation_id)
    }catch(err){
        generateError(resp,err)
    }
    return resp
}

DataManager.prototype.deleteConstraint = async function(msg){
    var resp = extractBasic(msg);
    try{
        await deleteInstance(msg.user_id,msg.constraint_id)
    }catch(err){
        generateError(resp,err)
    }
    return resp
}


//获取当前可以有推荐的节点,和该节点能够推荐的关系数量
//会产生大量的时间开销，避免频繁调用
DataManager.prototype.getGlobalRecommend = async function(msg){
    let resp = extractBasic(msg);
    let cypher = `MATCH (p:Project {name: "${msg.project}"})
    Match (u:User) Where id(u) = ${msg.user_id}
    Match (u) - [:refer] -> (i:Inst) <- [:has] - (p)
    Match (r:Relation) - [] -> (i) Where not ( (u) - [:refer] -> (r) 
                                                Or (u) - [:reject] -> (:Inst) <- [*0..] - (r) ) 
    Return id(i) as instId, count(r) as relaNum`;
    try{
        let res = await readCypher(cypher);
        resp['insts'] = {};
        for(let rec of res.records){
            resp['insts'][rec.get('instId')] = rec.get('relaNum')
        }
    }catch(err){
        generateError(resp,err)
    }
    return resp;
};

DataManager.prototype.getGlobalEntityRecommend = async function(msg){
    let resp = extractBasic(msg);
    let rcmd_num;
    if(msg.recommend_num) rcmd_num = msg.recommend_num;
    else rcmd_num = 100;
    let cypher = `Match (p:Project {name: "${msg.project}"}) - [:has] -> (et:EType) where not et:Union
    Match (u:User) Where id(u) = ${msg.user_id}
    Optional Match (e:Entity) - [:inst_of] - (et) Where not (u) - [:refer] - (e)
    Optional Match (ou:User) - [:refer] -> (e:Entity)
    With et,e,count(ou) as referCount 
    Order by referCount DESC
    Return id(et) as typeId ,collect(distinct e)[0..${rcmd_num}] as insts`;
    try{
        let result = {};
        let res = await readCypher(cypher);
        for(let rec of res.records){
            let entities = [];
            for(let e of rec.get('insts')){
                entities.push(extractInst(e));
            }
            result[rec.get('typeId')] = entities;
        }
        resp['entities'] = result;
    }catch(err){
        generateError(resp,err)
    }
    return resp;
}

DataManager.prototype.getRecommend = async function(msg){
    let session = driver.session();
    let resp = extractBasic(msg);
    try{
        await session.readTransaction(async (txc)=>{
            let cypher = `Match (i:Inst) Where id(i) = ${msg.inst_id}
            Match (u:User) Where id(u) = ${msg.user_id}
            Match (r:Relation) - [] -> (i) Where not ( (u) - [:refer] -> (r) 
                                                        Or (u) - [:reject] -> (:Inst) <- [*0..] - (r) ) 
            Match (r) - [role] -> (ri:Inst)
            Match (r) - [:inst_of] - (rt:RType)
            Optional Match (ou:User) - [:refer] -> (r)
            Optional Match (oru:User) - [:reject] -> (r)
            Return r,id(rt) as typeId, collect(distinct[role,ri]) as roles, (count(distinct ou)-count(distinct oru)) as priority
            Order by priority DESC LIMIT 200`;
            let res = await txc.run(cypher);
            resp["relations"] = dealRelationQueryGroup(res);
            //Todo 获取当前模型中没有的实体/关系的具体描述，存在可能的效率问题，需要根据具体的情境优化
            cypher = `Match (i:Inst) Where id(i) = ${msg.inst_id}
            Match (u:User) Where id(u) = ${msg.user_id}
            Match (r:Relation) - [] -> (i)
            Match (reci:Inst) <- [*1..] - (r) Where not ( (u) - [:refer] -> (reci) Or (u) - [:reject] -> (reci) )
            Match (reci) - [:inst_of] - (recit:Type)
            Optional Match (reci) - [role] -> (recri:Inst)
            Return reci,id(recit) as typeId, collect(distinct[role,recri]) as roles`
            res = await txc.run(cypher);
            let rela_res = {records:[]}
            let entity_res = {records:[]}
            for(let inst of res.records){
                if (inst.get("reci").labels[1]=="Relation") rela_res.records.push(inst);
                else entity_res.records.push(inst);
            }
            resp["rec_relations"] = dealRelationQueryGroup(rela_res);
            resp["rec_entities"] = dealEntityQueryGroup(entity_res)
        })
    }catch(err){
        generateError(resp,err)
    }
    return resp;
};

DataManager.prototype.citeRecommendInst = async function(msg){
    var resp = extractBasic(msg);
    try{
        let cypher = `Match (i:Inst) Where id(i) = ${msg.inst_id}
            Match (u:User) Where id(u) = ${msg.user_id}
            Match (relatedI:Inst) <- [*0..] - (i) Where not (u) - [:refer] -> (relatedI)
            Create (u) - [:refer {name:relatedI.name}] -> (relatedI)
            Return id(relatedI) as instId`
            /* 方便日志记录考虑，有效率影响
            Match (relatedI:Inst) <- [*0..] - (i)
            Merge (u) - [:refer] -> (relatedI)`;*/
        let res = await writeCypher(cypher);
        resp.inst_ids = [];
        for(let inst of res.records){
            resp.inst_ids.push(inst.get('instId'))
        }
    }catch(err){
        generateError(resp,err)
    }
    return resp
};

//这个函数为citeRecommendInst的特化，效率有所提高
DataManager.prototype.citeEntity = async function(msg){
    var resp = extractBasic(msg);
    let cypher =  `Match(u:User) Where id(u) = ${msg.user_id}
    Match(i:Entity) Where id(i) = ${msg.entity_id} and not (u) - [] -> (i)
    Create (u) - [:refer {name:i.name}]  -> (i)
    Return i`;
    try{
        let res = await writeCypher(cypher);
        if(res.records.length==0) throw Error("所引用的实例不存在，或已经被引用/拒绝");
    }catch(err){
        generateError(resp,err);
    };
    return resp;
};

DataManager.prototype.rejectInst = async function(msg){
    var resp = extractBasic(msg);
    let cypher =  `Match(u:User) Where id(u) = ${msg.user_id}
    Match(i:Inst) Where id(i) = ${msg.inst_id} and not (u) - [:refer] -> (i)
    Merge (u) - [:reject] -> (i)
    Return i`;
    try{
        let res = await writeCypher(cypher)
        if(res.records.length==0) throw Error("拒绝的实例不存在，或该实例还在当前知识图谱中");
    }catch(err){
        generateError(resp,err)
    };
    return resp;
};

//这个函数仅仅修改当前用户对于该Inst的名称认知，并不修改该Inst的实际name
DataManager.prototype.renameInst = async function(msg){
    var resp = extractBasic(msg);
    let cypher =  `Match(u:User) Where id(u) = ${msg.user_id}
    Match(i:Inst) Where id(i) = ${msg.inst_id}
    Match (u) - [ref:refer] -> (i)
    Set ref.name = "${msg.inst_name}"
    Return i`;
    //Todo 当已经没有用户使用原来的refer名称的时候，更换Inst的name
    try{
        let res = await writeCypher(cypher)
        if(res.records.length==0) throw Error("更名的实例不存在");
    }catch(err){
        generateError(resp,err)
    };
    return resp;
};




module.exports = DataManager;