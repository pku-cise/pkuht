"use strict"
const {extractBasic,readCypher,writeCypher,driver,generateError} = require("./utils");
const con = require('./const')
function DataManager(){}


let getTypeProperties = async function(type_id,txc){
    let cypher = `Match (t:Type) Where id(t)=${type_id}
    Optional match (t) - [hp:has_property] - (st:SymbolType)
    Return t,collect (distinct[hp,st]) as properties`;
    let res = await txc.run(cypher);
    let properties={};
    let type = res.records[0];
    for(let property of type.get("properties")){
        if(property[0] === null) continue;
        properties[property[0].properties.name]={
            "type_id":property[1].identity,
            "type_name":property[1].properties.name
        }
    }
    return properties;
}
DataManager.prototype.createEntityType = async function (msg) {
    var session = driver.session();
    var resp = extractBasic(msg);
    try{
        var cypher = `MATCH (p:Project {name: "${msg.project}"})
        MERGE (p)-[:has]-(t:Type:EType {name: "${msg.entity_type}"})
        With t
        Optional Match (u) - [rej:reject] -> (t) Where id(u)=${msg.user_id}
        Delete rej
        RETURN id(t) AS nodeId`;
        await session.writeTransaction(async txc=>{
            let res=await txc.run(cypher);
            resp['entity_type_id'] = res.records[0].get('nodeId')
            resp["properties"] = await getTypeProperties(resp["entity_type_id"],txc);
        })

    }catch(err){
        generateError(resp,err);
    }finally {
        await session.close()
    }
    return resp
}

DataManager.prototype.createRelationType = async function (msg) {
    var session = driver.session();
    var resp = extractBasic(msg);
    resp["roles"] = msg.roles;
    resp["role_types"] = []
    try{
        await session.writeTransaction(async txc=>{
            let cypher = `MATCH (p:Project {name: "${msg.project}"})
                    MATCH (p)-[:has]->(t:RType {name: "${msg.relation_type}"})
                    `;
            let i = 0;
            for(let [role,value] of Object.entries(msg.roles)){
                if(value.type_ids.length==1){
                    cypher+=`Match (t) - [:has_role {name: "${role}"}] -> (type${i}) Where id(type${i++}) =  ${value["type_ids"][0]} `;
                }
                else if(value.type_ids.length > 1)
                {
                    let ids = value.type_ids.sort((a,b)=>(a-b));
                    let name = JSON.stringify({
                        "operator": "or",
                        "ids": ids
                    });
                    cypher+=`Match (t) - [:has_role {name: "${role}"}] -> (type${i++} {name: '${name}'}) `;
                }
                else throw Error("type_ids格式错误");
            }

            cypher+=`Match (t)-[:has_role]-(type:Type)
                    Optional Match (u) - [rej:reject] - (t)
                    With t,count(type) as cnt,rej Where cnt=${i}
                    Delete rej
                    Return id(t) As typeId, rej`;
            let res = await txc.run(cypher);
            if(res.records.length != 0) { //查看有没有同样的关系类型（同样：指名称相同，所有的角色也相同）
                if(res.records[0].get("rej")==null){
                    throw Error("该关系已经存在");
                }
                resp['relation_type_id'] = res.records[0].get('typeId');
            }
            else{
                cypher = `MATCH (p:Project {name: "${msg.project}"})
                CREATE (p) - [:has] -> (t:Type:RType {name: "${msg.relation_type}"}) `;
                i = 0;
                for(let [role,value] of Object.entries(msg.roles)){
                    let minvalue = value["min"]===undefined?1:value["min"];
                    let maxvalue = value["max"]===undefined?1:value["max"];
                    
                    if(value.type_ids.length==1){
                        resp["roles"][role]["type_id"] = value["type_ids"][0]

                        cypher+=`WITH p,t
                        MATCH (p) - [:has] -> (type${i}:Type) where id(type${i}) = ${value["type_ids"][0]}
                        CREATE (t) - [:has_role {name: "${role}", min:${minvalue}, max:${maxvalue}}] -> (type${i++})`
                    }
                    else{
                        msg.type_ids = value.type_ids
                        let res_tmp = await this.createRoleType(msg)
                        //resp["roles"][role]["roletype"] = {
                        //    "type_id": res_tmp["type_id"],
                        //    "name": res_tmp["name"]
                        //}
                        resp["roles"][role]["type_id"] = res_tmp["type_id"]
                        let rt = {
                            "id": res_tmp["type_id"],
                            "type_ids": JSON.parse(res_tmp["name"])["ids"]
                        }
                        resp["role_types"].push(rt);

                        cypher+=`WITH p,t
                        MATCH (p) - [:has] -> (type${i}:Type) where id(type${i}) = ${res_tmp["type_id"]}
                        CREATE (t) - [:has_role {name: "${role}", min:${minvalue}, max:${maxvalue}}] -> (type${i++})`
                    }
                }
                cypher+='RETURN id(t) AS typeId'
                res=await txc.run(cypher)
                resp['relation_type_id'] = res.records[0].get('typeId')
            }
            resp["properties"] = await getTypeProperties(resp["relation_type_id"],txc);
        })
    }catch(err){
        generateError(resp,err);
    }finally {
        session.close();
    }
    return resp
}

DataManager.prototype.createSymbolType = async function(msg){
    let resp = extractBasic(msg);
    let cypher = `Create (s:SymbolType {name:"${msg.symbol_name}"})`;
    if(msg.symbol_type=='Enum'){
        cypher+=`Set s.type="Enum"
                 Set s.enum_values = ${JSON.stringify(msg.enum_values)}`
    }
    else if(msg.symbol_type=='Compound'){
        for(let [name,id] of Object.entries(msg.child_symbols)){
            cypher+=`Set s.type="Compound"
            With s Match (childs:SymbolType) where id(childs) = ${id}
            Create (s) - [:has_child_symbol {name:"${name}"}] -> (childs)`
        }
    }
    cypher += `Return id(s) as symbolId`
    try{
        let res =await writeCypher(cypher);
        resp.symbol_type_id = res.records[0].get("symbolId");
    }catch(err){
        generateError(resp,err);
    }
    return resp;
};

DataManager.prototype.rejectType = async function(msg){
    var resp = extractBasic(msg)
    let session = driver.session();
    try{
        if(con.MODEL_LAYER_MODE=="PERSONALIZE"){
            await session.writeTransaction(async txc=>{
                let cypher = `Match (p:Project {name: "${msg.project}"})
            Match (p) - [:has] - (t:Type) where id(t)=${msg.type_id} 
            Match (u) where id(u)=${msg.user_id}
            Optional Match (rt:RType) - [:has_role] -> (t) Where not (u)-[:reject]->(rt)
            Optional Match (u)- [:refer] -> (i:Inst) - [:inst_of] -> (t)
            Optional Match (et:EType) - [:bool] - > (t) Where not (u) - [:reject] -> (et)
            Return  t, rt, i, et`;
                let res = await txc.run(cypher);
                for(let rec of res.records){
                    if(rec._fields[2]!=null){
                        throw new Error("该类型尚有对应实例，请删除后重试");
                    }else if(rec._fields[1]!=null){
                        throw new Error("该类型为其它关系承担者类型，请删除对应关系类型");
                    }else if(rec._fields[3]!=null){
                        throw new Error("该类型有对应的逻辑类型依赖，请删除该逻辑类型")
                    }
                }
                cypher = `Match (p:Project {name: "${msg.project}"})
            Match (p) - [:has] - (t:Type) where id(t)=${msg.type_id} 
            Match (u) where id(u)=${msg.user_id}
            Create (u) - [:reject] -> (t)`
                await txc.run(cypher)
            })
        }else if(con.MODEL_LAYER_MODE=="SHARE"){
            await session.writeTransaction(async txc=>{
                let cypher = `Match (p:Project {name: "${msg.project}"})
                Match (p) - [:has] - (t:Type) where id(t)=${msg.type_id} 
                Match (u) where id(u)=${msg.user_id}
                Optional Match (rt:RType) - [:has_role] -> (t)
                Optional Match (i:Inst) - [:inst_of] -> (t)
                Optional Match (et:EType) - [:bool] - > (t)
                Return  t, rt, i, et`;
                let res = await txc.run(cypher);
                for(let rec of res.records){
                    if(rec._fields[2]!=null){
                        throw new Error("该类型尚有对应实例，请删除后重试");
                    }else if(rec._fields[1]!=null){
                        throw new Error("该类型为其它关系承担者类型，请删除对应关系类型");
                    }else if(rec._fields[3]!=null){
                        throw new Error("该类型有对应的逻辑类型依赖，请删除该逻辑类型")
                    }
                }
                cypher = `Match (p:Project {name: "${msg.project}"})
                Match (p) - [in:has] -> (t:Type) where id(t)=${msg.type_id} 
                Optional Match (t) - [out] -> () 
                Delete in,out,t`;
                await txc.run(cypher)
            })
        }

    }catch(err){
        generateError(resp,err);
    }finally {
        await session.close();
    }
    return resp;
}


DataManager.prototype.addTypeProperty = async function(msg){
    var resp = extractBasic(msg);
    let cypher = `Match (t:Type) where id(t) = ${msg.type_id} and not (t) - [:has_property{name:"${msg.property_name}"}] - (:SymbolType)
    Match (s:SymbolType) where id(s) = ${msg.symbol_type_id}
    Merge (t) - [:has_property {name:"${msg.property_name}"}] -> (s)
    Return t`;
    try{
        let res = await writeCypher(cypher);
        if(res.records.length==0){
            throw Error("添加失败，请检查是否该属性已存在")
        }
    }catch(err){
        generateError(resp,err);
    }
    return resp
};

DataManager.prototype.deleteTypeProperty = async function(msg){
    var session = driver.session();
    let resp = extractBasic(msg);
    let cypher = `Match (t:Type) where id(t) = ${msg.type_id}
    Match (i:Inst) - [:inst_of] -> (t) where not i.${msg.property_name} is NULL
    Return i`; //check inst是否有这一属性
    try{
        await session.writeTransaction(async txc => {
            let res = await txc.run(cypher);
            if(res.records.length!=0){
                throw Error("该属性尚有实例使用，请删除后再试");
            }
            cypher = `Match (t:Type) where id(t) = ${msg.type_id}
            Match (t) - [hp:has_property {name:"${msg.property_name}"}] - (st:SymbolType)
            Delete hp`;
            await txc.run(cypher);
        })
    }catch(err){
        generateError(resp,err);
    }
    return resp
}

let setEntityTypeSet = async function(msg){
    let id_symbol,type_name
    if(msg["operation"]==="createTypeUnion") {
        id_symbol = "or";
        type_name = "Union";
    }else if(msg["operation"]==="createTypeIntersection"){
        id_symbol = "and";
        type_name = "intersection";
    }
    let resp = extractBasic(msg)
    let ids = msg.type_ids.sort((a,b)=>(a-b));
    let name = JSON.stringify({
        "operator": id_symbol,
        "ids": ids
    });
    let cypher = `Match (p:Project {name: "${msg.project}"})
                  Merge (n:Type:EType:${type_name} {name:'${name}'})
                  Merge (p) - [:has] -> (n)`;
    for(let id in ids){
        cypher+=`With n
        Match (child${id}:EType) Where id (child${id})=${ids[id]}
        Merge (n) - [:bool] -> (child${id})`;
    }
    cypher+=`With n
    Match (u:User) where id(u) = ${msg.user_id}
    Optional Match (u) - [rej:reject] -> (n)
    Delete rej
    Return id(n) as typeId`;
    try{
        let res =await writeCypher(cypher);
        let records = res.records;
        if (records.length != 1)
        {
            throw new Error("db error");
        }
        resp.type_id = records[0].get("typeId");
        resp["name"] = name;
    }catch(err){
        generateError(resp,err);
    }
    return resp;
}

DataManager.prototype.createTypeIntersection = async function(msg){
    let resp = await setEntityTypeSet(msg);
    return resp;
}

DataManager.prototype.createTypeUnion = async function(msg){
    let resp = await setEntityTypeSet(msg);
    return resp;
}

let setTypeSet = async function(msg){
    let id_symbol = "or"
    let type_name = "Union"
    
    let resp = extractBasic(msg)
    let ids = msg.type_ids.sort((a,b)=>(a-b));
    let name = JSON.stringify({
        "operator": id_symbol,
        "ids": ids
    });
    let cypher = `Match (p:Project {name: "${msg.project}"})
                  Merge (n:Type:RoleType:${type_name} {name:'${name}'})
                  Merge (p) - [:has] -> (n)`;
    for(let id in ids){
        cypher+=`With n
        Match (child${id}:Type) Where id (child${id})=${ids[id]}
        Merge (n) - [:bool] -> (child${id})`;
    }
    cypher+=`With n
    Match (u:User) where id(u) = ${msg.user_id}
    Optional Match (u) - [rej:reject] -> (n)
    Delete rej
    Return id(n) as typeId`;
    try{
        let res =await writeCypher(cypher);
        let records = res.records;
        if (records.length != 1)
        {
            throw new Error("db error");
        }
        resp.type_id = records[0].get("typeId");
        resp["name"] = name;
    }catch(err){
        generateError(resp,err);
    }
    return resp;
}

DataManager.prototype.createRoleType = async function(msg){
    let resp = await setTypeSet(msg);
    return resp;
}



module.exports = DataManager;