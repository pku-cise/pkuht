
const neo4j = require('neo4j-driver')
const con = require('./const')
const utils = require('./utils')
const driver = neo4j.driver(con.DB_ADDRESS, neo4j.auth.basic('neo4j', con.DB_PASSWD),
                    { disableLosslessIntegers: true })

function DataManager(){}

let readCypher = async function(cypher){
    let session = driver.session();
    try{
        let res =  await session.readTransaction(async txc =>{
            let result = txc.run(cypher);
            return result;
        });
        return res;
    }finally {
        session.close();
    }
};

let writeCypher = async function(cypher){
    let session = driver.session();
    try{
        let res =  await session.writeTransaction(async txc =>{
            let result = txc.run(cypher);
            return result;
        })
        return res;
    }finally {
        session.close();
    }
}

DataManager.prototype.initDatabase = async function (msg) {
    var session = driver.session()
    var resp = utils.extractBasic(msg)
    try{
        await session.writeTransaction(async txc=>{
            await txc.run('MATCH (n) DETACH DELETE n')
            //内置属性类型:int/float/string
            let res = await txc.run('Create (s:SymbolType {name:"Int",type:"Basic"}) Return id(s) as symbolId');
            resp["int_id"] = res.records[0].get("symbolId");
            res = await txc.run('Create (s:SymbolType {name:"String",type:"Basic"}) Return id(s) as symbolId');
            resp["string_id"] = res.records[0].get("symbolId");
            res = await txc.run('Create (s:SymbolType {name:"Float",type:"Basic"}) Return id(s) as symbolId');
            resp["float_id"] = res.records[0].get("symbolId");
        })
        await session.writeTransaction(async txc=>{
            await txc.run('CREATE CONSTRAINT ON (u:User) ASSERT u.name IS UNIQUE');
            await txc.run('CREATE CONSTRAINT ON (p:Project) ASSERT p.name IS UNIQUE');
            await txc.run('CREATE CONSTRAINT ON (s:SymbolType) ASSERT s.name IS UNIQUE')
        })

    }catch(err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }finally {
        await session.close()
    }
    return resp
}

DataManager.prototype.createUser = async function (msg) {
    var session = driver.session();
    var resp = utils.extractBasic(msg);
    try{
        await session.writeTransaction(async txc=>{
            let cypher = `Match (u:User {name: '${msg.user}'}) Return u`;
            let res=await txc.run(cypher);
            if(res.records!=0) throw Error("该用户名已存在");
            cypher = `Create (u:User {name: '${msg.user}', passwd:'${msg.passwd}'})`;
            txc.run(cypher)
        })
    }catch(err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }finally {
        await session.close()
    }
    return resp
}

DataManager.prototype.checkUser = async function (msg) {
    var session = driver.session();
    var resp = utils.extractBasic(msg);
    try{
        let res = await session.run('MATCH (u:User {name : {name},\
                            passwd: {passwd}}) \
                            RETURN id(u) AS uid', {
            name: msg.user,
            passwd: msg.passwd,
        })
        let records = res.records;
        if (records.length != 0)
        {
            resp.msg = 'Success'
            resp.user_id = records[0].get('uid')
        }
        else
        {
            resp.error = true;
            resp.msg = "User not exists";
        }
    }catch(err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }finally {
        await session.close()
    }
    return resp
}

DataManager.prototype.createProject = async function (msg) {
    var resp = utils.extractBasic(msg);
    let cypher = `CREATE (p:Project {name : "${msg.project}"})`
    try{
        await writeCypher(cypher)
    }catch(err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }
    return resp
}

DataManager.prototype.getProjects = async function(msg){
    var resp = utils.extractBasic(msg);
    try{
        let cypher = `Match (p:Project)
        Match (u:User) Where id(u)=${msg.user_id}
        Return p.name as pname,u.name as uname`;
        let res = await readCypher(cypher);
        let plist = [];
        for(let p of res.records){
            plist.push(p.get("pname"));
        }
        resp["projects"] = plist;
        resp["user_name"] = res.records[0].get("uname")
    }catch(err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }
    return resp;
}


let getTypeProperties = async function(type_id,txc){
    let cypher = `Match (t:Type) Where id(t)=${type_id}
    Optional match (t) - [hp:has_property] - (st:SymbolType)
    Return t,collect (distinct[hp,st]) as properties`;
    let res = await txc.run(cypher);
    let properties={};
    let type = res.records[0];
    for(let property of type.get("properties")){
        if(property[0] === null) continue;
        properties[property[0].properties.name]={
            "type_id":property[1].identity,
            "type_name":property[1].properties.name
        }
    }
    return properties;
}
DataManager.prototype.createEntityType = async function (msg) {
    var session = driver.session();
    var resp = utils.extractBasic(msg);
    try{
        var cypher = `MATCH (p:Project {name: "${msg.project}"})
        MERGE (p)-[:has]-(t:Type:EType {name: "${msg.entity_type}"})
        With t
        Optional Match (u) - [rej:reject] -> (t) Where id(u)=${msg.user_id}
        Delete rej
        RETURN id(t) AS nodeId`;
        await session.writeTransaction(async txc=>{
            let res=await txc.run(cypher);
            resp['entity_type_id'] = res.records[0].get('nodeId')
            resp["properties"] = await getTypeProperties(resp["entity_type_id"],txc);
        })

    }catch(err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }finally {
        await session.close()
    }
    return resp
}

DataManager.prototype.createRelationType = async function (msg) {
    var session = driver.session();
    var resp = utils.extractBasic(msg);
    try{
        await session.writeTransaction(async txc=>{
            let cypher = `MATCH (p:Project {name: "${msg.project}"})
                    MATCH (p)-[:has]->(t:RType {name: "${msg.relation_type}"})
                    `;
            let i = 0;
            for(let [role,value] of Object.entries(msg.roles)){
                cypher+=`Match (t) - [:has_role {name: "${role}"}] -> (type${i}) Where id(type${i++}) =  ${value["type_id"]} `;
            }
            cypher+=`Match (t)-[:has_role]-(type:Type)
                    Optional Match (u) - [rej:reject] - (t)
                    With t,count(type) as cnt,rej Where cnt=${i}
                    Delete rej
                    Return id(t) As typeId, rej`;
            let res = await txc.run(cypher);
            if(res.records.length != 0) { //查看有没有同样的关系类型（同样：指名称相同，所有的角色也相同）
                if(res.records[0].get("rej")==null){
                    throw Error("该关系已经存在");
                }
                resp['relation_type_id'] = res.records[0].get('typeId');
            }
            else{
                cypher = `MATCH (p:Project {name: "${msg.project}"})
                CREATE (p) - [:has] -> (t:Type:RType {name: "${msg.relation_type}"}) `;
                i = 0;
                for(let [role,value] of Object.entries(msg.roles)){
                    let minvalue = value["min"]===undefined?1:value["min"];
                    let maxvalue = value["max"]===undefined?1:value["max"];
                    cypher+=`WITH p,t
                     MATCH (p) - [:has] -> (type${i}:Type) where id(type${i}) = ${value["type_id"]}
                     CREATE (t) - [:has_role {name: "${role}", min:${minvalue}, max:${maxvalue}}] -> (type${i++})`
                }
                cypher+='RETURN id(t) AS typeId'
                res=await txc.run(cypher)
                resp['relation_type_id'] = res.records[0].get('typeId')
            }
            resp["properties"] = getTypeProperties(resp["relation_type_id"],txc);
        })
    }catch(err){
        resp.error = true
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }finally {
        session.close();
    }
    return resp
}

DataManager.prototype.createSymbolType = async function(msg){
    let resp = utils.extractBasic(msg);
    let cypher = `Create (s:SymbolType {name:"${msg.symbol_name}"})`;
    if(msg.symbol_type=='Enum'){
        cypher+=`Set s.type="Enum"
                 Set s.enum_values = ${JSON.stringify(msg.enum_values)}`
    }
    else if(msg.symbol_type=='Compound'){
        for(let [name,id] of Object.entries(msg.child_symbols)){
            cypher+=`Set s.type="Compound"
            With s Match (childs:SymbolType) where id(childs) = ${id}
            Create (s) - [:has_child_symbol {name:"${name}"}] -> (childs)`
        }
    }
    cypher += `Return id(s) as symbolId`
    try{
        let res =await writeCypher(cypher);
        resp.symbol_type_id = res.records[0].get("symbolId");
    }catch(err){
        resp.error  = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }
    return resp;
};

DataManager.prototype.rejectType = async function(msg){
    var resp = utils.extractBasic(msg)
    let session = driver.session();
    try{
        if(con.MODEL_LAYER_MODE=="PERSONALIZE"){
            await session.writeTransaction(async txc=>{
                let cypher = `Match (p:Project {name: "${msg.project}"})
            Match (p) - [:has] - (t:Type) where id(t)=${msg.type_id} 
            Match (u) where id(u)=${msg.user_id}
            Optional Match (rt:RType) - [:has_role] -> (t) Where not (u)-[:reject]->(rt)
            Optional Match (u)- [:refer] -> (i:Inst) - [:inst_of] -> (t)
            Optional Match (et:EType) - [:bool] - > (t) Where not (u) - [:reject] -> (et)
            Return  t, rt, i, et`;
                let res = await txc.run(cypher);
                for(let rec of res.records){
                    if(rec._fields[2]!=null){
                        throw new Error("该类型尚有对应实例，请删除后重试");
                    }else if(rec._fields[1]!=null){
                        throw new Error("该类型为其它关系承担者类型，请删除对应关系类型");
                    }else if(rec._fields[3]!=null){
                        throw new Error("该类型有对应的逻辑类型依赖，请删除该逻辑类型")
                    }
                }
                cypher = `Match (p:Project {name: "${msg.project}"})
            Match (p) - [:has] - (t:Type) where id(t)=${msg.type_id} 
            Match (u) where id(u)=${msg.user_id}
            Create (u) - [:reject] -> (t)`
                await txc.run(cypher)
            })
        }else if(con.MODEL_LAYER_MODE=="SHARE"){
            await session.writeTransaction(async txc=>{
                let cypher = `Match (p:Project {name: "${msg.project}"})
                Match (p) - [:has] - (t:Type) where id(t)=${msg.type_id} 
                Match (u) where id(u)=${msg.user_id}
                Optional Match (rt:RType) - [:has_role] -> (t)
                Optional Match (i:Inst) - [:inst_of] -> (t)
                Optional Match (et:EType) - [:bool] - > (t)
                Return  t, rt, i, et`;
                let res = await txc.run(cypher);
                for(let rec of res.records){
                    if(rec._fields[2]!=null){
                        throw new Error("该类型尚有对应实例，请删除后重试");
                    }else if(rec._fields[1]!=null){
                        throw new Error("该类型为其它关系承担者类型，请删除对应关系类型");
                    }else if(rec._fields[3]!=null){
                        throw new Error("该类型有对应的逻辑类型依赖，请删除该逻辑类型")
                    }
                }
                cypher = `Match (p:Project {name: "${msg.project}"})
                Match (p) - [in:has] -> (t:Type) where id(t)=${msg.type_id} 
                Optional Match (t) - [out] -> () 
                Delete in,out,t`;
                await txc.run(cypher)
            })
        }

    }catch(err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }finally {
        session.close();
    }
    return resp;
}

let extractInst = function(inst)
{
    let result = {
        "id": inst.identity,
        "properties":{}
    };
    for(let [key,value] of Object.entries(inst.properties)){
        if(key=="name") result["name"] = value;
        else{
            //特殊符号处理
            value = value.replace("\n", "\\n")
            result["properties"][key] = JSON.parse(value)
        }

    }
    return result;
};

let refer = async function (userid, nodeid, txc, refer_name=null) {
    let cypher = `MATCH (u) WHERE id(u) = ${userid}
    MATCH (n) WHERE id(n) = ${nodeid} and not (u) - [:refer] - (n) 
    Optional Match (u) - [rej:reject] - (n) 
    Delete rej
    Create (u)-[:refer {name:n.name}]->(n)
    Return n.name`;
    let res = await txc.run(cypher)
    if(res.records.length==0){
        throw Error(`添加的实例已经在当前的知识图谱中,id为${nodeid}`);
    }
}

DataManager.prototype.createEntity = async function (msg) {
    var session = driver.session();
    var resp = utils.extractBasic(msg);
    try{
        await session.writeTransaction(async txc => {
            let cypher = `MATCH (p:Project {name: "${msg.project}"})
            MATCH (p)-[:has]->(et:EType) Where id(et)=${msg.entity_type_id}
            Match (:User) - [:refer {name: "${msg.entity_name}"}] -> (e:Entity) - [:inst_of]->(et) 
            RETURN DISTINCT e AS entity`;
            let res=await txc.run(cypher);
            let entity,result;
            if(res.records.length>1){
                throw Error(`存在重复的实例，请联系管理员(助教)`);
            }
            else if(res.records.length>=1){
                entity = res.records[0].get('entity');
                result = extractInst(entity);
                resp = Object.assign(resp,result);
            }else{
                cypher = `MATCH (p:Project {name: "${msg.project}"})
                MATCH (p)-[:has]->(et:EType) Where id(et)=${msg.entity_type_id}
                Create (e:Inst:Entity {name:"${msg.entity_name}"}) - [:inst_of] -> (et)
                Create (p) - [:has] -> (e)
                Return e AS entity`;
                res = await txc.run(cypher);
                if(res.records.length == 0) throw Error("创建错误，请检查该实体类型是否存在");
                entity = res.records[0].get('entity');
                result = extractInst(entity);
                result["name"] = msg.entity_name;
                resp = Object.assign(resp,result);
            }
            await refer(msg.user_id, result["id"], txc, refer_name = msg.entity_name)
        })

    }catch(err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }finally {
        await session.close()
    }
    return resp
}

DataManager.prototype.createRelation = async function (msg) {
    var session = driver.session();
    var resp = utils.extractBasic(msg);
    try{
        let cypher = `MATCH (p:Project {name: "${msg.project}"})\
        MATCH (p)-[:has]->(r:Relation)\
        MATCH  (r)-[:inst_of] -> (re:RType) Where id(re)=${msg.relation_type_id} `;
        let i = 0;
        for(let role of msg.roles){
            cypher+=`MATCH (r) - [:${role.role}] -> (inst${i}:Inst) WHERE id(inst${i++})=${role.inst_id} `
        }
        //确保匹配到的是该关系的所有角色
        cypher+=`Match (r) - [] -> (inst:Inst)
        With r,count(inst) as cnt
        Where cnt = ${i}
        Return r as relation`;
        await session.writeTransaction(async txc => {
            let res=await txc.run(cypher);
            let relation;
            if(res.records.length==0) {
                let cypher = `MATCH (p:Project {name: "${msg.project}"})\
                MATCH (p)-[:has] -> (re:RType) Where id(re)=${msg.relation_type_id}\
                CREATE (p)-[:has]->(r:Inst:Relation)\
                CREATE (r)-[:inst_of] -> (re) `
                let i = 0;
                for(let role of msg.roles){
                    //Todo 增加类型校验
                    cypher+=`WITH r\
                            MATCH (e${i}:Inst) WHERE id(e${i})=${role.inst_id}\
                            CREATE (r) - [:${role.role}] -> (e${i++}) `
                }
                cypher+='RETURN r AS relation';
                res = await txc.run(cypher);
                relation = res.records[0].get('relation')
            }else {
                relation = res.records[0].get('relation')
            }
            let result = extractInst(relation)
            resp = Object.assign(resp,result)
            await refer(msg.user_id, result['id'], txc)
        })

    }catch(err){
        utils.generateError(resp,err);
    }finally {
        await session.close()
    }
    return resp
}

DataManager.prototype.createConstraint = async function (msg) {
    var session = driver.session();
    var resp = utils.extractBasic(msg);
    try{
        let cypher = ``;

        if(("precond_item_list" in msg) && (msg.precond_item_list.length > 0)){
            for(let precond_item of msg.precond_item_list){
                if(("precond_list" in precond_item) && (precond_item.precond_list.length > 0)){
                    for(let precond of precond_item.precond_list){
                        if(("args" in precond) && (precond.args.length > 1)){}
                        else {throw Error("createConstraint: precond args length error.");}

                        if("opt" in precond) {}
                        else {throw Error("createConstraint: precond NO opt.");}
                    }
                }else{
                    throw Error("createConstraint: precond_item NO precond_list.");
                }
            }
        }else{
            throw Error("createConstraint: NO precond_item_list.");
        }

        if(("postcond_item_list" in msg) && (msg.postcond_item_list.length > 0)){
            for(let postcond_item of msg.postcond_item_list){
                if(("postcond_list" in postcond_item) && (postcond_item.postcond_list.length > 0)){
                    for(let postcond of postcond_item.postcond_list){
                        if(("args" in postcond) && (postcond.args.length > 1)){}
                        else {throw Error("createConstraint: postcond args length error.");}

                        if("opt" in postcond) {}
                        else {throw Error("createConstraint: postcond NO opt.");}
                    }
                }else{
                    throw Error("createConstraint: postcond_item NO postcond_list.");
                }
            }
        }else{
            throw Error("createConstraint: NO postcond_item_list.");
        }


        await session.writeTransaction(async txc => {

            cypher = `MATCH (p:Project {name: "${msg.project}"}) \
            CREATE (p)-[:has]->(c:Constraint:Inst) `

            cypher += `Set c.precond_item_list = '${JSON.stringify(msg.precond_item_list)}' `
            cypher += `Set c.postcond_item_list = '${JSON.stringify(msg.postcond_item_list)}' `

            for(let precond_item of msg.precond_item_list){
                for(let precond of precond_item.precond_list){
                    for(let arg of precond.args){
                        if(arg.type=="node"){
                            cypher += `with p,c \
                            MATCH (i:Inst) where id(i)=${arg.inst_id} \ 
                            Create (c)-[:precond]->(i) `
                        }
                        else if(arg.type=="number"){
                        }
                        //else if(arg.type=="int"){}
                        //else if(arg.type=="float"){}
                        else if(arg.type=="bool"){
                        }
                        else if(arg.type=="patient"){
                        }
                        else if(arg.type=="string"){
                        }
                    }
                }
            }

            for(let postcond_item of msg.postcond_item_list){
                for(let postcond of postcond_item.postcond_list){
                    for(let arg of postcond.args){
                        if(arg.type=="node"){
                            cypher += `with p,c \
                            MATCH (i:Inst) where id(i)=${arg.inst_id} \ 
                            Create (c)-[:postcond]->(i) `
                        }
                        else if(arg.type=="number"){
                        }
                        //else if(arg.type=="int"){}
                        //else if(arg.type=="float"){}
                        else if(arg.type=="bool"){
                        }
                        else if(arg.type=="patient"){
                        }
                        else if(arg.type=="string"){
                        }
                    }
                }
            }
            cypher += `RETURN c as constraint `

            let res = await txc.run(cypher);

            let record = res.records[0];
            let constraint = extractInst(record.get("constraint"))

            await refer(msg.user_id, constraint['id'], txc)

            resp["id"] = constraint["id"];
            resp["precond_item_list"] = constraint["properties"]["precond_item_list"]
            resp["postcond_item_list"] = constraint["properties"]["postcond_item_list"]
        })

    }catch(err){
        resp.error = true;
        resp.msg = err;
    }finally {
        await session.close()
    }
    return resp
}

let getEntityTypes = async function(project,user_id,txc){
    let cypher = `Match (p:Project {name: "${project}"})
    Match (u:User) where id(u) = ${user_id}
    Match (p) - [:has] -> (et:EType) Where not ( (u) - [:reject] -> (et) )
    Optional match (et) - [hp:has_property] - (st:SymbolType) 
    Return et,collect (distinct[hp,st]) as properties`;
    let res = await txc.run(cypher)
    let ets = {};
    for (let et of res.records){
        ets[et._fields[0].identity]={
            "name":et._fields[0].properties.name,
            "properties":{}
        };
        for(let property of et.get("properties")){
            if(property[0] === null) continue;
            ets[et._fields[0].identity]["properties"][property[0].properties.name]={
                "type_id":property[1].identity,
                "type_name":property[1].properties.name
            }
        }
    }
    return ets;
}

let getRelationTypes = async function(project,user_id,txc){
    let cypher = `Match (p:Project {name: "${project}"})
    Match (u:User) where id(u) = ${user_id}
    Match (p) - [:has] -> (rt:RType) Where not ( (u) - [:reject] -> (rt) ) 
    Match (rt) - [hr:has_role] -> (et:Type)
    Optional match (rt) - [hp:has_property] - (st:SymbolType)
    Return rt,collect(distinct [hr, et]) AS roles,collect (distinct[hp,st]) as properties`
    let res = await txc.run(cypher)
    let rts = {};
    for (let rt of res.records){
        let rt_obj  = {
            "name":rt._fields[0].properties.name,
            "roles":{},
            "properties":{},
        }
        for(let role of rt._fields[1]){
            rt_obj["roles"][role[0].properties.name] = {
                "type_id":role[1].identity,
                "type_name":role[1].properties.name,
                "min": role[0].properties.min,
                "max": role[0].properties.max,
            }
        }
        for(let property of rt.get("properties")){
            if(property[0] === null) continue;
            rt_obj["properties"][property[0].properties.name]={
                "type_id":property[1].identity,
                "type_name":property[1].properties.name
            }
        }
        rts[rt._fields[0].identity]=rt_obj
    }
    return rts;
}

//这个函数被用于抽取对一组entity的查询信息，其cypher返回语句应类似：
//Return e,id(et) as typeId, ref.name as refName（这一项为可选项，被用于用户给实体的别名)`
let dealEntityQueryGroup= function(res)
{
    let ens = {};
    for (let en of res.records){
        let result = extractInst(en._fields[0]);
        ens[result["id"]]={
            "type_id":en._fields[1],
            //当抽取用户个人知识图谱时，这一项为用户认为这个实体的名字
            "name":en.keys.length>2&&en.keys[2]==='refName'? en._fields[2]:result["name"],
            "properties":result["properties"]
        }
    }
    return ens;
}

//这个函数被用于抽取对一组relation的查询信息，其cypher返回语句应类似：
//Return r,id(rt) as typeId, collect(distinct[role,i]) as roles`
let dealRelationQueryGroup = function(res){
    let relas = {};
    for (let rela of res.records){
        let result  = extractInst(rela._fields[0])
        let rela_inst = {
            "type_id":rela.get("typeId"),
            "properties":result["properties"],
            "roles":[]
        }
        for(let role of rela._fields[2]){
            rela_inst["roles"].push({"role":role[0].type,"inst_id":role[1].identity})
        }
        //Todo 妥协方案，用来获取推荐优先级，之后会用列表中的顺序替代
        if(rela.has("priority")) rela_inst["priority"] = rela.get("priority");
        relas[result["id"]] = rela_inst
    }
    return relas;
}

//Return c as constraint;
let dealConstraintQueryGroup = function(res){
    let constraints = []
    for (let record of res.records)
    {
        let constraint = extractInst(record.get("constraint"))
        let res = {}
        res["id"] = constraint["id"]
        res["precond_item_list"] = constraint["properties"]["precond_item_list"]
        res["postcond_item_list"] = constraint["properties"]["postcond_item_list"]
        constraints.push(res)
    }
    return constraints
}


let getEntities = async function(project,userid,txc){
    let cypher = `Match (p:Project {name: "${project}"})
    Match (u:User) Where id(u)=${userid}
    Match (p) - [:has] -> (e:Entity) <- [ref:refer] - (u)
    Match (e) - [:inst_of] -> (et:EType)
    Return e,id(et) as typeId, ref.name as refName`
    let res = await txc.run(cypher)
    return dealEntityQueryGroup(res);
}

let getRelations = async function(project,userid,txc){
    let cypher = `Match (p:Project {name: "${project}"})
    Match (u:User) Where id(u)=${userid}
    Match (p) - [:has] -> (r:Relation) <- [:refer] - (u)
    Match (r) - [:inst_of] -> (rt:RType)
    Match (r) - [role] -> (e:Inst)
    Return r,id(rt) as typeId, collect(distinct[role,e]) as roles`;
    let res = await txc.run(cypher)
    return dealRelationQueryGroup(res);
}

let getConstraints = async function(project,userid,txc){
    let cypher = `Match (p:Project {name: "${project}"})
    Match (u:User) Where id(u)=${userid}
    Match (p) - [:has] -> (c:Constraint) <- [:refer] - (u)
    Return c as constraint `
    let res = await txc.run(cypher)
    return dealConstraintQueryGroup(res);
}

let getSymbolTypes = async function(txc){
    let cypher = `Match (s:SymbolType)
    Optional match (s) - [hc:has_child_symbol] -> (cs:SymbolType)
    Return s,collect(distinct[hc.name,id(cs)]) as childs`;
    let symbol_types = {};
    let res = await txc.run(cypher);
    for(let st of res.records){
        let id = st.get("s").identity;
        let symbol_type = st.get("s").properties;
        for(let child of st.get("childs")){
            if(child[0]===null) break;
            if(symbol_type.child_symbols===undefined) symbol_type.child_symbols={};
            symbol_type.child_symbols[child[0]] = child[1];
        }
        symbol_types[id] = symbol_type;
    }
    return symbol_types;
}

DataManager.prototype.getGraph = async function(msg){
    var session = driver.session();
    var resp = utils.extractBasic(msg);
    try{
        await session.readTransaction(async (txc)=>{
            resp["entity_types"] = await getEntityTypes(msg.project,msg.user_id,txc);
            resp["relation_types"] = await getRelationTypes(msg.project,msg.user_id,txc);
            resp["entities"] = await getEntities(msg.project, msg.user_id, txc);
            resp["relations"] = await getRelations(msg.project, msg.user_id, txc);
            resp["constraints"] = await getConstraints(msg.project, msg.user_id, txc)
            resp["symbol_types"] = await getSymbolTypes(txc)
        })
    }catch(err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = err;
    }finally {
        await session.close()
    }
    return resp;
}

let getSubgraphEntities = async function(project,userid,entities,txc){
    let cypher = ``
    for(let i=0; i<entities.length; i++)
        for(let j=i+1; j<entities.length; j++)
        {
            cypher = `Match (p:Project {name: "${project}"})
            Match (u:User) Where id(u)=${userid}
            Match (p) - [:has] -> (e:Entity) <- [ref:refer] - (u)
            Match (e) - [:inst_of] -> (et:EType)
            Return e,id(et) as typeId, ref.name as refName`
        }




}

//msg["entities"] = [{"id": 112}, ..]
DataManager.prototype.getSubgraph = async function(msg){
    var session = driver.session();
    var resp = utils.extractBasic(msg);
    try{
        await session.readTransaction(async (txc)=>{
            let relationTypes = getRelationTypes(msg.project,msg.user_id,txc);

            resp["entities"] = await getSubgraphEntities(msg.project, msg.user_id, msg.entities, txc);
            //resp["relations"] = await getSubgraphRelations(msg.project, msg.user_id, txc);
            //resp["constraints"] = await getSubgraphConstraints(msg.project, msg.user_id, txc);
        })
    }catch(err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }finally {
        await session.close()
    }
    return resp;
}


DataManager.prototype.addTypeProperty = async function(msg){
    var resp = utils.extractBasic(msg);
    let cypher = `Match (t:Type) where id(t) = ${msg.type_id} and not (t) - [:has_property{name:"${msg.property_name}"}] - (:SymbolType)
    Match (s:SymbolType) where id(s) = ${msg.symbol_type_id}
    Merge (t) - [:has_property {name:"${msg.property_name}"}] -> (s)
    Return t`;
    try{
        let res = await writeCypher(cypher);
        if(res.records.length==0){
            throw Error("添加失败，请检查是否该属性已存在")
        }
    }catch(err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }
    return resp
};

DataManager.prototype.deleteTypeProperty = async function(msg){
    var session = driver.session();
    let resp = utils.extractBasic(msg);
    let cypher = `Match (t:Type) where id(t) = ${msg.type_id}
    Match (i:Inst) - [:inst_of] -> (t) where not i.${msg.property_name} is NULL
    Return i`; //check inst是否有这一属性
    try{
        await session.writeTransaction(async txc => {
            let res = await txc.run(cypher);
            if(res.records.length!=0){
                throw Error("该属性尚有实例使用，请删除后再试");
            }
            cypher = `Match (t:Type) where id(t) = ${msg.type_id}
            Match (t) - [hp:has_property {name:"${msg.property_name}"}] - (st:SymbolType)
            Delete hp`;
            await txc.run(cypher);
        })
    }catch(err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }
    return resp
}

DataManager.prototype.setInstanceProperty = async function(msg){
    var session = driver.session();
    var resp = utils.extractBasic(msg);
    try{
        let cypher = `Match (n:Inst) where id(n)=${msg["inst_id"]} `
        for(let key in msg["properties"]){
            let value = msg["properties"][key];
            if(value===null){
                cypher += `Set n.${key} = null `;
                continue;
            }
            cypher += `Set n.${key} = '${JSON.stringify(value)}'`
        }
        await session.run(cypher)
    }catch(err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }finally {
        await session.close()
    }
    return resp;
};

let deleteInstance = async function(user_id,inst_id){
    //有向图，边的方向
    let cypher = `Match (u:User) - [ref:refer] - (i:Inst)
                      Where id(u)=${user_id} and id(i)=${inst_id}
                      Optional Match (u) - [ref_r:refer] -> (r:Relation) - [*1..] -> (i) 
                      Optional Match (u) - [ref_c:refer] -> (c:Constraint) - [*1..] -> (i)
                      Delete ref,ref_r,ref_c
                      Return ref,ref_r,ref_c`
    let res =await writeCypher(cypher)
    if(res.records.length==0) {
        throw new Error(`refer not exists`);
    }
};

DataManager.prototype.deleteEntity = async function(msg){
    var resp = utils.extractBasic(msg);
    try{
        await deleteInstance(msg.user_id,msg.entity_id)
    }catch(err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }
    return resp
}

DataManager.prototype.deleteRelation = async function(msg){
    var resp = utils.extractBasic(msg);
    try{
        await deleteInstance(msg.user_id,msg.relation_id)
    }catch(err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }
    return resp
}

DataManager.prototype.deleteConstraint = async function(msg){
    var resp = utils.extractBasic(msg);
    try{
        await deleteInstance(msg.user_id,msg.constraint_id)
    }catch(err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }
    return resp
}

let setTypeSet = async function(msg){
    let id_symbol,type_name
    if(msg["operation"]==="getTypeUnion") {
        id_symbol = "or";
        type_name = "Union";
    }else if(msg["operation"]==="getTypeIntersection"){
        id_symbol = "and";
        type_name = "intersection";
    }
    let resp = utils.extractBasic(msg)
    let ids = msg.type_ids.sort((a,b)=>(a-b));
    let name = JSON.stringify({
        "operator": id_symbol,
        "ids": ids
    });
    let cypher = `Match (p:Project {name: "${msg.project}"})
                  Merge (n:Type:EType:${type_name} {name:'${name}'})
                  Merge (p) - [:has] -> (n)`;
    for(let id in ids){
        cypher+=`With n
        Match (child${id}:EType) Where id (child${id})=${ids[id]}
        Merge (n) - [:bool] -> (child${id})`;
    }
    cypher+=`With n
    Match (u:User) where id(u) = ${msg.user_id}
    Optional Match (u) - [rej:reject] -> (n)
    Delete rej
    Return id(n) as typeId`;
    try{
        let res =await writeCypher(cypher);
        let records = res.records;
        if (records.length != 1)
        {
            throw new Error("db error");
        }
        resp.type_id = records[0].get("typeId");
        resp["name"] = name;
    }catch(err){
        resp.error  = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }
    return resp;
}
DataManager.prototype.getTypeIntersection = async function(msg){
    let resp = await setTypeSet(msg);
    return resp;
}

DataManager.prototype.getTypeUnion = async function(msg){
    let resp = await setTypeSet(msg);
    return resp;
}

//获取当前可以有推荐的节点,和该节点能够推荐的关系数量
//会产生大量的时间开销，避免频繁调用
DataManager.prototype.getGlobalRecommend = async function(msg){
    let resp = utils.extractBasic(msg);
    let cypher = `MATCH (p:Project {name: "${msg.project}"})
    Match (u:User) Where id(u) = ${msg.user_id}
    Match (u) - [:refer] -> (i:Inst) <- [:has] - (p)
    Match (r:Relation) - [] -> (i) Where not ( (u) - [:refer] -> (r) 
                                                Or (u) - [:reject] -> (:Inst) <- [*0..] - (r) ) 
    Return id(i) as instId, count(r) as relaNum`;
    try{
        let res = await readCypher(cypher);
        resp['insts'] = {};
        for(let rec of res.records){
            resp['insts'][rec.get('instId')] = rec.get('relaNum')
        }
    }catch(err){
        resp.error  = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }
    return resp;
};

DataManager.prototype.getGlobalEntityRecommend = async function(msg){
    let resp = utils.extractBasic(msg);
    let rcmd_num;
    if(msg.recommend_num) rcmd_num = msg.recommend_num;
    else rcmd_num = 100;
    let cypher = `Match (p:Project {name: "${msg.project}"}) - [:has] -> (et:EType) where not et:Union
    Match (u:User) Where id(u) = ${msg.user_id}
    Optional Match (e:Entity) - [:inst_of] - (et) Where not (u) - [:refer] - (e)
    Optional Match (ou:User) - [:refer] -> (e:Entity)
    With et,e,count(ou) as referCount 
    Order by referCount DESC
    Return id(et) as typeId ,collect(distinct e)[0..${rcmd_num}] as insts`;
    try{
        let result = {};
        let res = await readCypher(cypher);
        for(let rec of res.records){
            let entities = [];
            for(let e of rec.get('insts')){
                entities.push(extractInst(e));
            }
            result[rec.get('typeId')] = entities;
        }
        resp['entities'] = result;
    }catch(err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }
    return resp;
}

DataManager.prototype.getRecommend = async function(msg){
    let session = driver.session();
    let resp = utils.extractBasic(msg);
    try{
        await session.readTransaction(async (txc)=>{
            let cypher = `Match (i:Inst) Where id(i) = ${msg.inst_id}
            Match (u:User) Where id(u) = ${msg.user_id}
            Match (r:Relation) - [] -> (i) Where not ( (u) - [:refer] -> (r) 
                                                        Or (u) - [:reject] -> (:Inst) <- [*0..] - (r) )
            Match (r) - [role] -> (ri:Inst)
            Match (r) - [:inst_of] - (rt:RType)
            Optional Match (ou:User) - [:refer] -> (r)
            Optional Match (oru:User) - [:reject] -> (r)
            Return r,id(rt) as typeId, collect(distinct[role,ri]) as roles, (count(distinct ou)-count(distinct oru)) as priority
            Order by priority DESC LIMIT 200`;
            let res = await txc.run(cypher);
            resp["relations"] = dealRelationQueryGroup(res);
            //Todo 获取当前模型中没有的实体/关系的具体描述，存在可能的效率问题，需要根据具体的情境优化
            cypher = `Match (i:Inst) Where id(i) = ${msg.inst_id}
            Match (u:User) Where id(u) = ${msg.user_id}
            Match (r:Relation) - [] -> (i)
            Match (reci:Inst) <- [*1..] - (r) Where not ( (u) - [:refer] -> (reci) Or (u) - [:reject] -> (reci) )
            Match (reci) - [:inst_of] - (recit:Type)
            Optional Match (reci) - [role] -> (recri:Inst)
            Return reci,id(recit) as typeId, collect(distinct[role,recri]) as roles`
            res = await txc.run(cypher);
            let rela_res = {records:[]}
            let entity_res = {records:[]}
            for(let inst of res.records){
                if (inst.get("reci").labels[1]=="Relation") rela_res.records.push(inst);
                else entity_res.records.push(inst);
            }
            resp["rec_relations"] = dealRelationQueryGroup(rela_res);
            resp["rec_entities"] = dealEntityQueryGroup(entity_res)
        })
    }catch(err){
        resp.error  = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }
    return resp;
};

DataManager.prototype.citeRecommendInst = async function(msg){
    var resp = utils.extractBasic(msg);
    try{
        let cypher = `Match (i:Inst) Where id(i) = ${msg.inst_id}
            Match (u:User) Where id(u) = ${msg.user_id}
            Match (relatedI:Inst) <- [*0..] - (i) Where not (u) - [:refer] -> (relatedI)
            Create (u) - [:refer {name:relatedI.name}] -> (relatedI)
            Return id(relatedI) as instId`
            /* 方便日志记录考虑，有效率影响
            Match (relatedI:Inst) <- [*0..] - (i)
            Merge (u) - [:refer] -> (relatedI)`;*/
        let res = await writeCypher(cypher);
        resp.inst_ids = [];
        for(let inst of res.records){
            resp.inst_ids.push(inst.get('instId'))
        }
    }catch(err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    }
    return resp
};

//这个函数为citeRecommendInst的特化，效率有所提高
DataManager.prototype.citeEntity = async function(msg){
    var resp = utils.extractBasic(msg);
    let cypher =  `Match(u:User) Where id(u) = ${msg.user_id}
    Match(i:Entity) Where id(i) = ${msg.entity_id} and not (u) - [] -> (i)
    Create (u) - [:refer {name:i.name}]  -> (i)
    Return i`;
    try{
        let res = await writeCypher(cypher);
        if(res.records.length==0) throw Error("所引用的实例不存在，或已经被引用/拒绝");
    }catch(err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    };
    return resp;
};

DataManager.prototype.rejectInst = async function(msg){
    var resp = utils.extractBasic(msg);
    let cypher =  `Match(u:User) Where id(u) = ${msg.user_id}
    Match(i:Inst) Where id(i) = ${msg.inst_id} and not (u) - [:refer] -> (i)
    Merge (u) - [:reject] -> (i)
    Return i`;
    try{
        let res = await writeCypher(cypher)
        if(res.records.length==0) throw Error("拒绝的实例不存在，或该实例还在当前知识图谱中");
    }catch(err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    };
    return resp;
};

//这个函数仅仅修改当前用户对于该Inst的名称认知，并不修改该Inst的实际name
DataManager.prototype.renameInst = async function(msg){
    var resp = utils.extractBasic(msg);
    let cypher =  `Match(u:User) Where id(u) = ${msg.user_id}
    Match(i:Inst) Where id(i) = ${msg.inst_id}
    Match (u) - [ref:refer] -> (i)
    Set ref.name = "${msg.inst_name}"
    Return i`;
    //Todo 当已经没有用户使用原来的refer名称的时候，更换Inst的name
    try{
        let res = await writeCypher(cypher)
        if(res.records.length==0) throw Error("更名的实例不存在");
    }catch(err){
        resp.error = true;
        resp.msg = {message:err.name=="Neo4jError"?err.name:err.message};
        resp.errordetail = JSON.stringify(err);
    };
    return resp;
};




module.exports = DataManager