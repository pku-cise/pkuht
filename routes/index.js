var express = require('express');
var router = express.Router();
const con = require('../models/const')
const logger = require("../logger_config");

let dm = new (require('../models/dm'))();
let inst_dm = new (require('../models/inst'))();
let model_dm = new (require('../models/model'))();
let project_dm = new (require('../models/project'))();

const admin_operation=["createProject","createEntityType","createRelationType","createSymbolType","getTypeIntersection","getTypeUnion","addTypeProperty","deleteTypeProperty"]
const read_operation = ["getProjects","getGraph","getGlobalRecommend","getGlobalEntityRecommend","getRecommend"];
/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
});

router.post('/api',async function(req, res, next) {
    let msg = req.body;
    let operation = msg["operation"];
    if(con.MODEL_LAYER_AUTH==="ADMIN_ONLY"&&admin_operation.indexOf(operation)!=-1){
        let resp = {};
        resp.error = true;
        resp.msg = {message:"权限不足，请联系管理员"};
        res.send(resp);
        return;
    }
    let resp = await dm[operation](msg=msg);
    let log = {
        "request": msg,
        "response": resp
    };
    if(resp["error"]){
        logger.error(JSON.stringify(log));
        logger.error(log.response.error_detail);
    }else if(resp["operation"]&&read_operation.indexOf(resp["operation"])!=-1){
        logger.debug(JSON.stringify(log));
    }else{
        logger.info(JSON.stringify(log));
    }
    res.send(resp);
})


router.get('/api',async function(req, res, next) {
    console.log(req);
    res.send({
        "a":"b"
    });
});

let normal_read = async function(req, res, dm){
    let msg = req.body;
    let operation = msg["operation"];
    if(dm[operation]===undefined){
        let resp = {};
        resp.error = true;
        resp.msg = {message:"请求错误"};
        res.send(resp);
        return;
    }
    let resp = await dm[operation](msg=msg);
    let log = {
        "request": msg,
        "response": resp
    };
    if(resp["error"]){
        logger.error(JSON.stringify(log));
        logger.error(log.response.error_detail);
    }else{
        logger.info(JSON.stringify(log));
    }
    res.send(resp);
}

let normal_write = async function(req, res, dm){
    let msg = req.body;
    let operation = msg["operation"];
    if(dm[operation]===undefined){
        let resp = {};
        resp.error = true;
        resp.msg = {message:"请求错误"};
        res.send(resp);
        return;
    }
    let resp = await dm[operation](msg=msg);
    let log = {
        "request": msg,
        "response": resp
    };
    if(resp["error"]){
        logger.error(JSON.stringify(log));
        logger.error(log.response.error_detail);
    }else{
        logger.debug(JSON.stringify(log));
    }
    res.send(resp);
}

router.post('/inst',async function(req, res, next) {
    await normal_write(req,res,inst_dm);
})
router.get('/inst',async function(req, res, next) {
    await normal_read(req,res,inst_dm);
});
router.patch('/inst',async function(req, res, next) {
    await normal_read(req,res,inst_dm);
});

router.post('/model',async function(req, res, next) {
    if(con.MODEL_LAYER_AUTH==="ADMIN_ONLY"){
        let resp = {};
        resp.error = true;
        resp.msg = {message:"权限不足，请联系管理员"};
        res.send(resp);
        return;
    }
    await normal_write(req,res,model_dm);
})
router.get('/model',async function(req, res, next) {
    await normal_read(req,res,model_dm);
});
router.patch('/model',async function(req, res, next) {
    await normal_read(req,res,model_dm);
});

router.post('/project',async function(req, res, next) {
    if(con.MODEL_LAYER_AUTH==="ADMIN_ONLY"){
        let resp = {};
        resp.error = true;
        resp.msg = {message:"权限不足，请联系管理员"};
        res.send(resp);
        return;
    }
    await normal_write(req,res,project_dm);
})
router.get('/project',async function(req, res, next) {
    await normal_read(req,res,project_dm);
});
router.patch('/project',async function(req, res, next) {
    await normal_read(req,res,project_dm);
});

module.exports = router;
