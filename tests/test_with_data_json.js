const DataManager = require('../models/dm');
const fs = require('fs');
const { exit } = require('process');
dm = new DataManager()
let func_name, user_id;
let msg = {
    project: "test_project",
    user: "test_user2",
    operation: func_name,
    passwd: "test_passwd",
}
async function test_dm(){
    msg.operation = func_name;
    let resp = await dm[func_name](msg=msg)
    if(resp["error"]) {
        console.log(msg)
        console.log(resp)
    }
    return resp
}

async function test_with_data(){
    //let duplicated_type = ["相关疾病","相关症状","检查"]
    func_name = "initDatabase";
    await test_dm();
    func_name = "createUser";
    await test_dm();
    msg["user"] = "test_user";
    await test_dm();
    func_name = "checkUser"
    let resp = await test_dm()
    msg["user_id"] = resp["user_id"]
    func_name = "createProject"
    await test_dm()
    console.time("write")
    func_name = "createEntityType"
    let entity_types={"disease":1, "department":2, "symptom":3, "drug":4, "check":5, "food":6}
    for(let et in entity_types){
        msg["entity_type"] = et;
        resp = await test_dm();
        entity_types[et] = resp["entity_type_id"]
    }
 
    let relations = {};
    func_name = "createRelationType"
    let data = fs.readFileSync('json_preprocessing/relations_type_json.txt', 'UTF-8');
    let lines = data.split(/\r?\n/) // 正则
    for (const line of lines) {
        let strs = line.split(" ");
        //if(duplicated_type.includes(strs[1])) continue;
        msg['relation_type'] = strs[1];
        msg['roles'] = {}
        msg['roles'][strs[0]]={
            "type_id":entity_types[strs[0]]
        };
        if(strs[2]!=strs[0]){
            msg['roles'][strs[2]]={
                "type_id":entity_types[strs[2]]
            };
        }
        resp = await test_dm();
        relations[strs[0]+strs[1]+strs[2]] = resp["relation_type_id"]
    }
    
    let entitymap = {};
    // func_name = "createEntity"
    data = fs.readFileSync('json_preprocessing/entities_json.txt', 'UTF-8');
    lines = data.split(/\r?\n/)
    for (const line of lines) {
        func_name = "createEntity"
        let strs = line.split(" #分1隔5符# ");
        msg['entity_name'] = strs[0];
        msg['entity_type_id'] = entity_types[strs[1]];
        resp = await test_dm();
        entitymap[strs[0]] = resp["id"];
        
        if(strs[1]=="disease"){    
            func_name = "setInstanceProperty"
            let pos = line.indexOf(" #分1隔5符# ")
            pos = line.indexOf(" #分1隔5符# ", pos+9)
            let str = line.substr(pos+9)
            //str = `{"nihao": "难受'男生", "test": []}`
            //特殊符号处理 '替换为%27
            str = str.replace("\n", "\\n")
            str = str.replace("'", "%27")
            msg["inst_id"] = resp["id"];
            msg["properties"] = JSON.parse(str)
            resp = await test_dm();
        }
    }
    console.log("end entity")
    func_name = "createRelation"
    data = fs.readFileSync('json_preprocessing/relations_json.txt', 'UTF-8');
    lines = data.split(/\r?\n/)
    for (const line of lines) {
        let strs = line.split(" #分1隔5符# ");
       // if(duplicated_type.includes(strs[4])) continue;
        msg['relation_type_id'] = relations[strs[1]+strs[4]+strs[3]];
        msg['roles'] = [];
        msg['roles'].push({"role":strs[5],"inst_id":entitymap[strs[0]]});
        msg['roles'].push({"role":strs[6],"inst_id":entitymap[strs[2]]});
        resp = await test_dm();
    }
    console.log("end")
    
    console.timeEnd("write")
    console.time("read")
    func_name = "getGraph"
    resp = await test_dm();
    console.log(Object.keys(resp.entities).length)
    console.log(Object.keys(resp.relations).length)
    console.timeEnd("read")
    console.log(resp)
    console.log(resp["entities"])
}

test_with_data();
