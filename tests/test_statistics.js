"use strict"
const {extractBasic,readCypher,writeCypher,driver,generateError} = require("../models/utils");


async function test(){
    let dict = {
        "242": 0,
        "013": 0,
        "031": 0
    };
    var session = driver.session();
    let cypher = `Match (u:User)-[r:refer]->(i:Relation)<-[:has]-(p:Project {name:"多学科医学知识图谱"})
    Return u.passwd as userid,id(r) as referid,id(i) as instid`;
    //cypher = `Match (u:User) Return u.passwd as userid`;
    let res = await readCypher(cypher);
    let dic = {}
    for(let rec of res.records){
       let userid = parseInt(rec.get("userid"));
       let referid = parseInt(rec.get("referid"));
       let instid = parseInt(rec.get("instid"));
       if(!dic[instid]) dic[instid]=[];
       dic[instid].push([referid,userid])
    }
    for(let [inst,list] of Object.entries(dic)){
        let min = 10000000;
        let id = 0;
        for(let ent of list){
            if(ent[0]<min){
                id = ent[1];
                min = ent[0];
            }
        }
        if(list.length<=1) continue;
        if(id>=24200&&id<=24299) {
            dict["242"]+=list.length-1;
        }
        else if (id>=1300&&id<=1399) dict["013"]+=list.length-1;
        else if (id>=3100&&id<=3199) dict["031"]+=list.length-1;
    }
    for(let key in dict){
        dict[key]*=1.5
    }
    console.log(dict);

    session.close();

}


test()

