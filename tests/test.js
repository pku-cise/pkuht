// let dm = require('./dm');
// dm = new dm();
let dm = new (require('../models/dm'))();
const inst_dm = new (require('../models/inst'))();
const model_dm = new (require('../models/model'))();
const project_dm = new (require('../models/project'))();
const fs = require('fs');
let onuse_dm = dm;
let func_name, user_id;
let msg = {
    project: "test_project",
    user: "test_user",
    operation: func_name,
    passwd: "test_passwd",
}
async function test_dm(){
    msg.operation = func_name;
    let resp = await onuse_dm[func_name](msg=msg)
    if(resp["error"]) {
        console.log(msg);
        console.log(resp);
    }
    return resp
}
async function test(){
    //-----------知识图谱初始化------------------
    func_name = "initDatabase";
    let resp = await test_dm();
    let int_id = resp["int_id"];
    let float_id = resp["float_id"];
    let string_id = resp["string_id"];


    //------------用户管理测试--------------------

    func_name = "createUser"
    await test_dm()
    func_name = "checkUser"
    resp = await test_dm()
    msg["user_id"] = resp["user_id"]
    
    let testuserid0 = msg["user_id"]

    func_name = "createProject"
    await test_dm()


    //------------模型层测试--------------------
    func_name = "createSymbolType";
    msg["symbol_name"] = "区间";
    msg["symbol_type"] = "Compound";
    msg["child_symbols"] = {
        "最大值": float_id,
        "最小值": float_id,
        "单位": string_id
    }
    resp=await test_dm();
    let interval_id = resp["symbol_type_id"];
    msg["symbol_name"] = "药物类型";
    msg["symbol_type"] = "Enum";
    msg["enum_values"] = ["中药","西药","中成药"];
    resp = await test_dm();
    let enum_id = resp["symbol_type_id"];


    func_name = "createEntityType"
    msg["entity_type"] = "药物"
    resp = await test_dm()
    resp = await test_dm()
    let entity_type_id1 = resp["entity_type_id"]; //药物的类型id
    msg["entity_type"] = "疾病"
    resp = await test_dm();
    let entity_type_id2 = resp["entity_type_id"]; //疾病的类型id、
    msg["entity_type"] = "症状"
    resp = await test_dm();
    let entity_type_id3 = resp["entity_type_id"];
    msg["entity_type"] = "微生物"
    resp = await test_dm();
    let entity_type_id4 = resp["entity_type_id"];
    msg["entity_type"] = "诊疗"
    resp = await test_dm();
    
    onuse_dm = model_dm
    func_name = "createTypeUnion";
    msg["type_ids"] = [entity_type_id2,entity_type_id3];
    resp = await test_dm();
    let entity_uniontype_id1 = resp["type_id"]


    msg["type_ids"] = [entity_type_id2,entity_type_id3];
    resp = await test_dm();
    //console.log(resp)
    
    //onuse_dm = inst_dm
    //func_name = "getGraph"
    //resp = await test_dm();
    //console.log(resp)
    onuse_dm = model_dm
    func_name = "createRelationType"
    msg["relation_type"] = "治疗";
    let roles = {
        "药物": {
            "type_ids":[entity_type_id1],
            "min":1,
            "max":5,
        },
        "治疗目标": {
            "type_ids":[entity_uniontype_id1],
        },
    };
    msg["roles"] = roles;
    resp = await test_dm();
    //console.log(resp)
    
    let relation_type_id1 = resp["relation_type_id"]; //治疗关系的id
    msg["relation_type"] = "并发症";
    roles = {
        "疾病":{
            "type_ids":[entity_type_id2],
            "min": 2,
            "max": 10,
        }
    }
    msg["roles"] = roles;
    resp = await test_dm();
    //console.log(resp)

    let relation_type_id2 = resp["relation_type_id"] //并发症关系的id
    msg["relation_type"] = "治疗禁忌";
    roles = {
        "治疗方案":{
            "type_ids": [relation_type_id1],
        },
        "药物":{
            "type_ids": [entity_type_id1],
        }
    }
    msg["roles"] = roles;
    resp = await test_dm();

    //console.log(resp);

    let relation_type_id3 = resp["relation_type_id"];
    msg["relation_type"] = "病原导致";
    roles = {
        "后果":{
            "type_ids": [entity_uniontype_id1],
        },
        "病原":{
            "type_ids": [entity_type_id4],
        }
    }
    msg["roles"] = roles;
    resp = await test_dm();
    let relation_type_id4 = resp["relation_type_id"];

    //console.log(resp)
    msg["relation_type"] = "药物治疗";
    roles = {
        "药物":{
            "type_ids": [entity_type_id1],
        },
        "治疗目标":{
            "type_ids": [entity_type_id2, entity_type_id4, relation_type_id4],
        }
    }
    msg["roles"] = roles;
    resp = await test_dm();    
    //console.log(resp["role_types"])
    //return;

    msg["relation_type"] = "test";
    roles = {
        "治疗目标":{
            "type_ids": [entity_type_id2, relation_type_id4],
        }
    }
    msg["roles"] = roles;
    resp = await test_dm();  

    console.log(resp);
    console.log(resp["roles"])
    console.log(resp["role_types"])

    onuse_dm = inst_dm
    func_name = "getGraph"
    resp = await test_dm();    
    //console.log(resp["relation_types"]);
    //for(let res of resp.relation_types){
    //    console.log(res.name, res.roles)
    //}
    console.log("-------------------qjc",resp.role_types)

    return 

    onuse_dm = model_dm;
    func_name = "addTypeProperty";
    msg["type_id"] = relation_type_id1;
    msg["property_name"] = "用量";
    msg["symbol_type_id"] = interval_id;
    await test_dm();
    msg["type_id"] = entity_type_id1;
    msg["property_name"] = "用量";
    msg["symbol_type_id"] = interval_id;
    await test_dm();
    msg["property_name"] = "不良反应";
    msg["symbol_type_id"] = string_id;
    await test_dm();
    msg["property_name"] = "药物编号";
    msg["symbol_type_id"] = int_id;
    await test_dm();
    msg["property_name"] = "浮点数编号";
    msg["symbol_type_id"] = float_id;
    await test_dm();
    msg["property_name"] = "类别";
    msg["symbol_type_id"] = enum_id;
    await test_dm();
    
    //------------------------角色类型测试-------------------
    onuse_dm = model_dm;
    console.log("----------role type test start-----------");
    msg["project"] = "test_project";
    msg["user_id"] = testuserid0;
    func_name = "createRoleType";
    //func_name = "createTypeUnion";

    msg["type_ids"] = [relation_type_id1, entity_type_id2]
    resp = await test_dm();
    let role_type_id0 = resp["type_id"];
    console.log(resp);

    //relation 应用
    func_name = "createRelationType";
    msg["relation_type"] = "qjc"
    msg["roles"] = {
        "test0":{"type_id":role_type_id0, "max": 10, "min": 1}, 
        "test1":{"type_id":entity_type_id3}
    }
    resp = await test_dm();
    console.log(resp);

    //getgraph
    onuse_dm = inst_dm
    func_name = "getGraph"
    resp = await test_dm();
    console.log(resp);    

    console.log("----------------role type test end-----------");

    // func_name = "createEntityType"
    // msg["entity_type"] = "药物"
    // resp = await test_dm()
    // console.log(JSON.stringify(resp,null,'\t'))

    //---------------------实例层测试--------------
    onuse_dm = dm
    func_name = "createEntity";
    msg["entity_type_id"] = entity_type_id2;
    msg["entity_name"] = "肺炎"
    resp =await test_dm()
    let entity1Id = resp["id"]
    msg["entity_name"] = "肺结核"
    resp = await test_dm()

    let testdataid2 = resp["id"];

    let entity3Id = resp["id"]
    msg["entity_name"] = "左氧氟沙星"
    msg["entity_type_id"] = entity_type_id1;
    resp = await test_dm()
    let entity2Id = resp["id"]
    msg["entity_name"] = "利福平"
    resp = await test_dm();
    let entity4Id = resp["id"]
    func_name = "setInstanceProperty"
    msg["inst_id"] = entity2Id;
    msg["properties"] = {
        "不良反应": "腹泻"
    }
    resp = await test_dm();

    func_name = "createRelation"
    msg["relation_type_id"] = relation_type_id2; //并发症关系的id
    roles = [];
    roles.push({
        "role":"疾病",
        "inst_id":entity1Id
    })
    roles.push({
        "role":"疾病",
        "inst_id":entity3Id
    })
    msg["roles"] = roles;
    resp = await test_dm();

    let testdataid0 = resp["id"];

    msg["relation_type_id"]=  relation_type_id1;
    roles = [];
    roles.push({
        "role":"治疗目标",
        "inst_id":entity1Id
    })
    roles.push({
        "role":"药物",
        "inst_id":entity2Id
    })
    msg["roles"] = roles;
    resp = await test_dm();

    let testdataid1 = resp["id"];

    let relation_id1 = resp["id"];
    roles = [];
    roles.push({
        "role":"治疗目标",
        "inst_id":entity3Id
    })
    roles.push({
        "role":"药物",
        "inst_id":entity2Id
    })
    roles.push({
        "role":"药物",
        "inst_id":entity4Id
    })
    msg["roles"] = roles;
    resp = await test_dm();

    let testdataid3 = resp["id"]

    roles=[];
    msg["relation_type_id"] = relation_type_id3;
    roles.push({
        "role": "治疗方案",
        "inst_id": relation_id1
    });
    roles.push({
        "role": "药物",
        "inst_id": entity4Id,
    })
    msg["roles"] = roles;
    resp = await test_dm();
    let relation_id2 = resp["id"]

    func_name = "setInstanceProperty"
    msg["inst_id"] = entity2Id;
    msg["properties"] = {
        "不良反应": "腹泻"
    }
    resp = await test_dm()
    msg["inst_id"] = relation_id1;
    msg["properties"] = {
        "用量":{
            "最大值": 10,
            "最小值": 5,
            "单位": "mg/d"
        }
    }
    resp = await test_dm();

    onuse_dm = inst_dm;
    func_name = "getGraph";
    resp = await test_dm();

    // ----------------------推荐功能测试-------------------
    onuse_dm = project_dm;
    msg["user"] = "test_user2";
    func_name = "createUser";
    await test_dm();
    func_name = "checkUser";
    resp = await test_dm();
    msg["user_id"] = resp["user_id"];

    onuse_dm = inst_dm;
    func_name = "createEntity";
    msg["entity_type_id"] = entity_type_id1;
    msg["entity_name"] = "利福平";
    resp =await test_dm();
    let rentity_id = resp["id"];

    func_name = "renameInst";
    msg["inst_id"] = rentity_id;
    msg["inst_name"] = "利发霉素";
    resp = await test_dm();
    // msg["entity_name"] = "左氧氟沙星";
    // resp =await test_dm();
    // msg["entity_type_id"] = entity_type_id2;
    // msg["entity_name"] = "肺炎";
    // resp =await test_dm();
    // msg["entity_name"] = "肺结核";
    // resp =await test_dm();
    // entity4Id =resp["id"];
    func_name = "getGlobalRecommend";
    resp = await test_dm();

    func_name = "getGlobalEntityRecommend";
    resp = await test_dm();
    //console.log(JSON.stringify(resp,null,'\t'))

    func_name = "citeEntity";
    msg['entity_id'] = entity1Id;
    resp = await test_dm();


    func_name ="getRecommend";
    msg["inst_id"] = rentity_id;
    resp = await test_dm();
    console.log(JSON.stringify(resp,null,'\t'));

    func_name = "rejectInst";
    msg["inst_id"] = entity2Id;
    resp = await test_dm();

    func_name = "getRecommend";
    msg["inst_id"] = rentity_id;
    resp = await test_dm();


    func_name = "citeRecommendInst";
    for(let relation in resp.relations)
    {
        msg["inst_id"] = relation;
        resp = await test_dm();
    }
    func_name = "getGraph";
    resp = await test_dm();

    // ----------------------跨图谱功能测试-------------------
    onuse_dm = project_dm;
    func_name = "createProject";
    msg["project"] = "test_project2";
    await test_dm();
    onuse_dm = model_dm;
    func_name = "createEntityType";
    msg["entity_type"] = "药物"
    resp = await test_dm();
    console.assert(resp["entity_type_id"]!=entity_type_id1)


    // ----------------------约束功能测试----------------------
    onuse_dm = inst_dm;
    console.log("----------test Constraint start-----------");
    msg["project"] = "test_project";
    msg["user_id"] = testuserid0;
    func_name = "createConstraint";

    msg["precond_item_list"] = [{
        "precond_list":[{
            "args":[
                    {"arg_location": "left", "type": "patient", "property": "体重" },    
                    {"arg_location": "right", "type": "number", "number": "50", "unit": "kg" }
                ],
            "opt": "="
        },{
            "args":[
                    {"arg_location": "right", "type": "node", "inst_id": testdataid2, "property": "valid"},    
                    {"arg_location": "left", "type": "number", "number": 1.4, "unit": "g/ml"}
                ],
            "opt": ">"
        }]
    },{
        "precond_list":[{
            "args":[
                    {"arg_location": "left", "type": "patient", "property": "体重" },    
                    {"arg_location": "right", "type": "number", "number": "50", "unit": "kg" }
                ],
            "opt": "="   
        }]
    }]
    
    msg["postcond_item_list"] = [{
        "postcond_list":[{
            "args":[
                {"arg_location": "left", "type": "node", "inst_id": testdataid1, "property": "用量"},    
                {"arg_location": "right", "type": "number", "number": 10, "unit": "g/ml"}
                ],
            "opt": "="
        }]
    }]

    resp = await test_dm();
    console.log(resp);
    //console.log(resp["precond_item_list"][0]["precond_list"], "-----------------------test")    
    let testdatacons0 = resp["id"]

    func_name = "getGraph";
    resp = await test_dm();
    console.log(JSON.stringify(resp,null,'\t'));
    //console.log(resp["constraints"][0]["postcond_item_list"])
    console.log("----------test Constraint end-----------")

    // ----------------------删除功能测试-------------------
    func_name = "deleteRelation";
    msg["relation_id"] = testdataid3
    resp = await test_dm();
    console.log(resp);

    func_name = "deleteRelation";
    msg["relation_id"] = testdataid1
    resp = await test_dm();
    console.log(resp);

/*
    func_name = "deleteConstraint";
    msg["constraint_id"] = testdatacons0;
    resp = await test_dm();
    console.log(resp);
*/
    console.log("--------------end---------------")

    //msg["user_id"]
    msg["project"] = "test_project";
    onuse_dm = model_dm;
    func_name = "createRelationType";
    msg["relation_type"] = "治疗禁忌2";
    roles = {
        "治疗方案":{
            "type_ids": [relation_type_id1]
        },
        "药物":{
            "type_ids": [entity_type_id1]
        }
    }
    msg["roles"] = roles;
    resp = await test_dm();
    let relation_type_id_test = resp["relation_type_id"] //并发症关系的id
    func_name = "rejectType";
    msg["type_id"] = relation_type_id_test;
    resp = await test_dm();
    // console.log(JSON.stringify(resp,null,'\t'));
    // func_name = "deleteRelation"h
    // msg["relation_id"] = relation_id2;
    // await test_dm();
    // // func_name = "deleteEntity";
    // // msg["entity_id"] = entity2Id;
    // // await test_dm()
    // // msg["entity_id"] = entity4Id;
    // //await test_dm()
    // func_name = "rejectType";
    // msg["type_id"] = relation_type_id3;
    // resp = await test_dm();
    // func_name = "createRelationType";
    // msg["relation_type"] = "治疗禁忌";
    // roles = {
    //     "治疗方案":{
    //         "type_id": relation_type_id1,
    //     },
    //     "药物":{
    //         "type_id": entity_type_id1,
    //     }
    // }
    // msg["roles"] = roles;
    // resp = await test_dm();
    func_name = "getProjects";
    onuse_dm = project_dm;
    resp = await test_dm();
    // func_name = "deleteTypeProperty";
    // msg["type_id"] = entity_type_id1;
    // msg["property_name"] = "药物编号";
    // resp = await test_dm();

}


test();

/*Todo：
Corner1: 其他用户使用别名创建实体 //done
Corner2： 多个project对于类型的共同使用 //done
 */

