const DataManager = require('../models/dm');
const fs = require('fs');
dm = new DataManager()
let func_name, user_id;
let msg = {
    project: "多学科医学知识图谱",
    user: "test_user2",
    operation: func_name,
    passwd: "test_passwd",
}
async function test_dm(){
    msg.operation = func_name;
    let resp = await dm[func_name](msg=msg)
    if(resp["error"]) {
        console.log(msg)
        console.log(resp)
    }
    return resp
}

async function test_with_data(){
    //let duplicated_type = ["相关疾病","相关症状","检查"]
    func_name = "initDatabase";
    await test_dm();
    func_name = "createUser";
    await test_dm();
    msg["user"] = "test_user";
    await test_dm();
    func_name = "checkUser"
    let resp = await test_dm()
    msg["user_id"] = resp["user_id"]
    func_name = "createProject"
    await test_dm()
    console.time("write")
    func_name = "createEntityType"
    let entity_types={"症状":1,"疾病":2,"药物":3,"检查":4}
    for(let et in entity_types){
        msg["entity_type"] = et;
        resp = await test_dm();
        entity_types[et] = resp["entity_type_id"]
    }

    func_name = "getTypeUnion";
    msg["type_ids"] = [entity_types["症状"],entity_types["疾病"]];
    resp = await test_dm();
    entity_types["疾病or症状"] = resp["type_id"];

    let relations = {};
    let redirect = {};
    let reverse = {};
    let rtype_dict = {}
    func_name = "createRelationType"
    let data = fs.readFileSync('relationtypes.txt', 'UTF-8');
    let lines = data.split(/\r?\n/)
    for (const line of lines) {
        let strs = line.split(" ");
        //if(duplicated_type.includes(strs[1])) continue;
        if(strs.length==5){
            msg['relation_type'] = strs[2];
            msg['roles'] = {};
            msg['roles'][strs[1]]={
                "type_id":entity_types[strs[0]]
            };
            if(strs[3]!=strs[1]){
                msg['roles'][strs[3]]={
                    "type_id":entity_types[strs[4]]
                };
            }else{
                msg['roles'][strs[1]]['max']=10;
                msg['roles'][strs[1]]['min']=2;
            }
            resp = await test_dm();
            if(relations[strs[2]]===undefined) relations[strs[2]]={};
            relations[strs[2]][[strs[0]]] = strs[4];
            rtype_dict[strs[2]+strs[0]+strs[4]]=[resp["relation_type_id"],strs[1],strs[3]];
            //Todo 这里first role的type不唯一的话可能会出现问题
        }else if(strs.length==3){
            if(strs[1]=="reverse"){
                reverse[strs[0]] = strs[2];
            }else if(strs[1]=="re"){
                redirect[strs[0]] = strs[2];
            }
        }else{
            if(line!="") console.log("create relation type error")
        }
    }

    let entitymap = {};
    func_name = "createEntity"
    data = fs.readFileSync('totalentity.txt', 'UTF-8');
    lines = data.split(/\r?\n/)
    for (const line of lines) {

        console.log(line);
        let strs = line.split(" ");
        msg['entity_name'] = strs[0];
        msg['entity_type_id'] = entity_types[strs[1]];
        if(!msg['entity_type_id']) continue;
        resp = await test_dm();
        entitymap[strs[0]+strs[1]] = resp["id"];
    }

    func_name = "createRelation"
    data = fs.readFileSync('totalrelation.txt', 'UTF-8');
    lines = data.split(/\r?\n/)
    for (const line of lines) {
        console.log(line);
        let strs = line.split(" ");
       // if(duplicated_type.includes(strs[4])) continue;
        let first_role_type,first_role_name,second_role_type,second_role_name;
        let first_role_otype,second_role_otype;
        let relation_name = strs[2]
        if(reverse[relation_name]){
            relation_name = reverse[relation_name];
            first_role_name = strs[3];
            first_role_type = strs[4];
            first_role_otype= strs[4];
            second_role_name = strs[0];
            second_role_type = strs[1];
            second_role_otype= strs[1];
        }
        else{
            if(redirect[relation_name]) relation_name=redirect[relation_name];
            first_role_name = strs[0];
            first_role_type = strs[1];
            first_role_otype= strs[1];
            second_role_name = strs[3];
            second_role_type = strs[4];
            second_role_otype= strs[4];
        }
        if(!relations[relation_name]) continue;
        if(!relations[relation_name][first_role_type]){
            if((first_role_type=="疾病"||first_role_type=="症状")&&relations[relation_name]["疾病or症状"]){
                first_role_type = "疾病or症状";
            }
            else continue;
        }
        if(relations[relation_name][first_role_type]!=second_role_type){
            if(relations[relation_name][first_role_type].indexOf(second_role_type)!=-1)
                second_role_type=relations[relation_name][first_role_type];
            else continue;
        }
        let relation_inform_list = rtype_dict[relation_name+first_role_type+second_role_type];
        msg['relation_type_id'] = relation_inform_list[0];
        msg['roles'] = [];
        msg['roles'].push({"role":relation_inform_list[1],"inst_id":entitymap[first_role_name+first_role_otype]});
        msg['roles'].push({"role":relation_inform_list[2],"inst_id":entitymap[second_role_name+second_role_otype]});
        resp = await test_dm();
    }
    console.timeEnd("write")
    console.time("read")
    func_name = "getGraph"
    resp = await test_dm();
    console.log(Object.keys(resp.entities).length)
    console.log(Object.keys(resp.relations).length)
    console.timeEnd("read")
    //console.log(resp)
}

test_with_data();