const DataManager = require('../models/dm');
const fs = require('fs');
dm = new DataManager()
let func_name, user_id;
let msg = {
    project: "医学知识图谱From临床指南",
    user: "test_user",
    operation: func_name,
    passwd: "test_passwd",
}
async function test_dm(){
    msg.operation = func_name;
    let resp = await dm[func_name](msg=msg)
    if(resp["error"]) {
        console.log(msg)
        console.log(resp)
    }
    return resp
}

async function test_with_data(){
    func_name = "initDatabase";
    await test_dm();
    func_name = "createUser"
    await test_dm();

    func_name = "checkUser"
    let resp = await test_dm()
    msg["user_id"] = resp["user_id"]
    func_name = "createProject"
    await test_dm()
    console.time("write")
    func_name = "createEntityType"
    let entity_types={"疾病":1,
        "病理病因":2,
        "症状":3,
        "药物":4,
        "运动":5,
        "诊疗技术":6}
    for(let et in entity_types){
        msg["entity_type"] = et;
        resp = await test_dm();
        entity_types[et] = resp["entity_type_id"]
    }

    func_name = "getTypeUnion";
    msg["type_ids"] = [entity_types["症状"],entity_types["疾病"]];
    resp = await test_dm();
    entity_types["症状|疾病"] = resp["type_id"];

    msg["type_ids"] = [entity_types["病理病因"],entity_types["疾病"]];
    resp = await test_dm();
    entity_types["病理病因|疾病"] = resp["type_id"];

    let symbol_types = {}
    func_name = "getGraph";
    resp = await test_dm();

    for(let typeid in resp["symbol_types"]){
        symbol_types[resp["symbol_types"][typeid].name]=typeid;
    }

    func_name = "createSymbolType";
    msg["symbol_name"] = "数值区间";
    msg["symbol_type"] = "Compound";
    msg["child_symbols"] = {
        "最大值": symbol_types["String"],
        "最小值": symbol_types["String"],
    }
    resp=await test_dm();
    symbol_types[msg["symbol_name"]] = resp["symbol_type_id"];

    msg["symbol_name"] = "数字区间";
    msg["symbol_type"] = "Compound";
    msg["child_symbols"] = {
        "最大值": symbol_types["Int"],
        "最小值": symbol_types["Int"],
    }
    resp=await test_dm();
    symbol_types[msg["symbol_name"]] = resp["symbol_type_id"];

    msg["symbol_name"] = "性别类型";
    msg["symbol_type"] = "Enum";
    msg["enum_values"] = ["男","女"];
    resp = await test_dm();
    symbol_types[msg["symbol_name"]] = resp["symbol_type_id"];

    msg["symbol_name"] = "程度类型";
    msg["symbol_type"] = "Enum";
    msg["enum_values"] = ["极轻","轻","中","重","极重"];
    resp = await test_dm();
    symbol_types[msg["symbol_name"]] = resp["symbol_type_id"];

    let properties = {};
    let data = fs.readFileSync('c_properties.txt', 'UTF-8');
    let lines = data.split(/\r?\n/)
    for (const line of lines) {
        let strs = line.split("\t");
        if(strs.length==2){
            properties[strs[0]] = symbol_types[strs[1]];
        }
    }

    data = fs.readFileSync('c_rtypes.txt', 'UTF-8');
    lines = data.split(/\r?\n/)
    for (const line of lines) {
        let strs = line.split("\t");
        if(strs.length>=2){
            msg['relation_type'] = strs[0];
            msg['roles'] = {};
            strs.splice(0,1);
            for(let role of strs){
                let rolestrs=role.split('/')
                msg['roles'][rolestrs[0]]={
                    "type_id":entity_types[rolestrs[1]],
                    "max":rolestrs[2].split("+")[1],
                    "min":rolestrs[2].split("+")[0]
                }
            }
            func_name = "createRelationType";
            resp = await test_dm();
            let type_id = resp["relation_type_id"]
            func_name = "addTypeProperty";
            for(let p_type in properties){
                msg["type_id"] = type_id;
                msg["property_name"] = p_type;
                msg["symbol_type_id"] = properties[p_type];
                resp = await test_dm();
            }
        }
    }
    console.timeEnd("write")
    console.time("read")
    func_name = "getGraph"
    resp = await test_dm();
    console.log(Object.keys(resp.entities).length)
    console.log(Object.keys(resp.relations).length)
    console.timeEnd("read")
    //console.log(resp)
}

test_with_data();