const log4js = require('log4js');
const fs = require('fs');

const logDirectory = __dirname+'/logs';
fs.existsSync(logDirectory)||fs.mkdirSync(logDirectory);

var operationLogDirectory=__dirname+'/logs/operation'; //每日创建一个日志文件
fs.existsSync(operationLogDirectory)||fs.mkdirSync(operationLogDirectory);

log4js.configure({
    appenders: {
        default: {
            type: 'dateFile',   //每日
            filename: operationLogDirectory + '/default',
            pattern: '-yyyy-MM-dd.log',
            alwaysIncludePattern: true,
            encoding: 'utf-8'
        },

        /**
         * 指定debug追加器
         */
        debug: {
            type: 'dateFile',   //每日
            filename: operationLogDirectory + '/debug',
            pattern: '-yyyy-MM-dd.log',
            alwaysIncludePattern: true,
            encoding: 'utf-8'
        },
        debugFilter: {
            type: 'logLevelFilter', // 设置type为logLevelFilter
            appender: 'debug', // 指定追加器为debug
            level: 'debug', // 设置捕获日志的最低等级
            maxLevel: 'debug', // 设置捕获日志的最高等级
        },

        /**
         * 指定info追加器
         */
        info: {
            type: 'dateFile',   //每日
            filename: operationLogDirectory + '/info',
            pattern: '-yyyy-MM-dd.log',
            alwaysIncludePattern: true,
            encoding: 'utf-8'
        },
        infoFilter: {
            type: 'logLevelFilter',
            appender: 'info',
            level: 'info',
            maxLevel: 'info',
        },

        /**
         * 指定warn追加器
         */
        warn: {
            type: 'dateFile',   //每日
            filename: operationLogDirectory + '/warn',
            pattern: '-yyyy-MM-dd.log',
            alwaysIncludePattern: true,
            encoding: 'utf-8'
        },
        warnFilter: {
            type: 'logLevelFilter',
            appender: 'warn',
            level: 'warn',
            maxLevel: 'warn',
        },

        /**
         * 指定error追加器
         */
        error: {
            type: 'dateFile',   //每日
            filename: operationLogDirectory + '/error',
            pattern: '-yyyy-MM-dd.log',
            alwaysIncludePattern: true,
            encoding: 'utf-8'
        },
        errorFilter: {
            type: 'logLevelFilter',
            appender: 'error',
            level: 'error',
            maxLevel: 'fatal',
        },
    },

    /**
     * 分类也使用默认的分类，所有没有找到对应定义分类的日志，都会归为default分类
     */
    categories: {
        default: {
            appenders: ['debugFilter', 'infoFilter', 'warnFilter', 'errorFilter'], // 设置追加器为上面设置的filter，对应上面的各个过滤器
            level: 'debug', // 设置记录日志的最低等级为debug，也就是所有日志都要记录
        },
    }
});

module.exports = log4js.getLogger()